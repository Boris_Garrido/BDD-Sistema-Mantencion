CREATE OR ALTER PROCEDURE spModificarMaquina

    @idEquipo               INT,
    @idSector               INT,
    @nombreEquipo           VARCHAR(200),
    @marca                  VARCHAR(200),
    @anioFabri              VARCHAR(20),
    @modelo                 VARCHAR(200),
    @peso                   VARCHAR(40),
    @rendimiento            VARCHAR(200),
    @fechaRecepcion         VARCHAR(40),
    @medidas                VARCHAR(100),
    @consumoAgua            VARCHAR(200),
    @tipoMaquina            VARCHAR(200),
    @foto                   VARBINARY(MAX)
    AS

    BEGIN TRY

        DECLARE @respuesta  INT


        UPDATE  Maquina
        SET idSector            = @idSector,
            tipoMaquina         = @tipoMaquina,
            nombreEquipo        = @nombreEquipo,
            marca               = @marca,
            AnioFabricacion     = @anioFabri,
            modelo              = @modelo,
            Peso                = @peso,
            rendimiento         = @rendimiento,
            fechaRecepcion      = @fechaRecepcion,
            medidas             = @medidas,
            consumoAgua         = @consumoAgua,
            Foto                = @foto
        WHERE idMaquina = @idEquipo
        SET @respuesta = 1
        RETURN @respuesta;

    END TRY

    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta

    END CATCH
GO;

