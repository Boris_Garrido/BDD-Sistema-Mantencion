CREATE OR ALTER PROCEDURE spHistorialMaquinaParea
    @idEquipo           INT,
    @area               NVARCHAR(150)
    AS
    BEGIN TRY
        SELECT
            mm.idMantencionGeneral                      AS idmantencion,
            mm.fecha                                    AS fechaMantencion,
            mm.Descripcion                              AS descripcionMantencion,
            mm.estado                                   AS estado,
            mm.Tipo                                     AS tipoMantencion,
            mm.area                                     AS clasficacionMantencion,
            m.idMaquina                                 AS idMaquina,
            m.nombreEquipo                              AS nombreEquipo,
            m.marca                                     AS marcaEquipo,
            m.modelo                                    AS modeloEquipo,
            u.idUsuario                                 AS idUsuario,
            u.nombreUsuario                             AS nombreUsuario,
            s.idSector                                  AS idSector,
            s.nombreSector                              AS nombreSector,
            t.idTurno                                   AS idTurno,
            t.nombreTurno                               AS nombreTurno,
            t.nombreAlt                                 AS nombreTurnoAlt
        FROM MantencionGeneralMaquina mm
        JOIN Maquina m ON m.idMaquina = mm.idMaquina
        JOIN Usuario u on mm.idUsuario = u.idUsuario
        JOIN SectorPlanta s ON s.idSector = m.idSector
        JOIN turno t ON t.idturno = mm.idTurno
        WHERE m.idMaquina = @idEquipo
        AND   mm.area LIKE @area
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;



