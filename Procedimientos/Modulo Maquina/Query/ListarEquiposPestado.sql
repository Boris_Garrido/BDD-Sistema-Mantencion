CREATE OR ALTER PROCEDURE spListarEquiposPestado
    @estado         INT
    AS
    BEGIN TRY

        SELECT
            m.idMaquina                 AS idEquipo,
            m.nombreEquipo              AS nombreEquipo,
            m.marca                     AS marcaEquipo,
            m.tipoMaquina               AS clasificacionEquipo,
            m.Foto                      AS fotoEquipo,
            s.idSector                  AS idSector,
            s.nombreSector              AS nombreSector
        FROM Maquina m
        JOIN SectorPlanta s on m.idSector = s.idSector
        WHERE m.habilitado = @estado
    END TRY

    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;






