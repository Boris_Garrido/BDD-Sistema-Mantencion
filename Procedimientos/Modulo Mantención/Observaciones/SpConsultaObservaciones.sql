CREATE OR ALTER PROCEDURE spConsultaObservaciones
    @tipoConsulta           NVARCHAR(50)
    AS

    BEGIN TRY

        DECLARE @FechaFiltro    DATE

        IF(@tipoConsulta = 'Hoy')
            BEGIN
               SET @FechaFiltro = CAST(GETDATE() AS DATE);
            END
        ELSE IF(@tipoConsulta = 'Ayer')
            BEGIN
                SET @FechaFiltro = CAST(GETDATE() - 1 AS DATE);
            END

        SELECT
            ob.idObservacionSector              AS idObservacion,
            ob.fecha                            AS fechaObservacion,
            ob.Descripcion                      AS descripcionObservacion,
            ob.estado                           AS estadoObservacion,
            ob.tipo                             AS tipoObservacion,
            ob.area                             AS areaObservacion,
            ob.horaInicio                       AS horaInicioObservacion,
            ob.HoraTermino                      AS horaTerminoObservacion,
            ob.minutosUsados                    AS duracionObservacion,
            ob.Notificada                       AS observacionNotificada,
            ob.comentarioEstado                 AS comentariosObservacion,
            'Sector'                            AS clasificacionObservacion,
            sp.idSector                         AS idEquipoSector,
            sp.nombreSector                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno

        FROM ObservacionSector          ob
        JOIN SectorPlanta               SP  ON ob.idSector = SP.idSector
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.Notificada = 0
        AND CAST(ob.fecha AS DATE) = @FechaFiltro

        UNION ALL

        SELECT
            ob.idObservacionMaquina             AS idObservacion,
            ob.fecha                            AS fechaObservacion,
            ob.Descripcion                      AS descripcionObservacion,
            ob.estado                           AS estadoObservacion,
            ob.tipo                             AS tipoObservacion,
            ob.area                             AS areaObservacion,
            ob.horaInicio                       AS horaInicioObservacion,
            ob.HoraTermino                      AS horaTerminoObservacion,
            ob.minutosUsados                    AS duracionObservacion,
            ob.Notificada                       AS observacionNotificada,
            ob.comentarioEstado                 AS comentariosObservacion,
            'Maquina'                           AS clasificacionObservacion,
            mq.idMaquina                        AS idEquipoSector,
            mq.nombreEquipo                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno
        FROM ObservacionMaquina         ob
        JOIN Maquina                    mq  ON ob.idMaquina = mq.idMaquina
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.Notificada = 0
        AND CAST(ob.fecha AS DATE) = @FechaFiltro
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spConsultaObservacionesSemanal
    AS

    BEGIN TRY
        DECLARE @SemanaActual INT = DATEPART(WEEK, GETDATE());
        SELECT
            ob.idObservacionSector              AS idObservacion,
            ob.fecha                            AS fechaObservacion,
            ob.Descripcion                      AS descripcionObservacion,
            ob.estado                           AS estadoObservacion,
            ob.tipo                             AS tipoObservacion,
            ob.area                             AS areaObservacion,
            ob.horaInicio                       AS horaInicioObservacion,
            ob.HoraTermino                      AS horaTerminoObservacion,
            ob.minutosUsados                    AS duracionObservacion,
            ob.Notificada                       AS observacionNotificada,
            ob.comentarioEstado                 AS comentariosObservacion,
            'Sector'                            AS clasificacionObservacion,
            sp.idSector                         AS idEquipoSector,
            sp.nombreSector                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno

        FROM ObservacionSector          ob
        JOIN SectorPlanta               SP  ON ob.idSector = SP.idSector
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.Notificada = 0
        AND DATEPART(WEEK, ob.fecha) = @SemanaActual

        UNION ALL

        SELECT
            ob.idObservacionMaquina             AS idObservacion,
            ob.fecha                            AS fechaObservacion,
            ob.Descripcion                      AS descripcionObservacion,
            ob.estado                           AS estadoObservacion,
            ob.tipo                             AS tipoObservacion,
            ob.area                             AS areaObservacion,
            ob.horaInicio                       AS horaInicioObservacion,
            ob.HoraTermino                      AS horaTerminoObservacion,
            ob.minutosUsados                    AS duracionObservacion,
            ob.Notificada                       AS observacionNotificada,
            ob.comentarioEstado                 AS comentariosObservacion,
            'Maquina'                           AS clasificacionObservacion,
            mq.idMaquina                        AS idEquipoSector,
            mq.nombreEquipo                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno
        FROM ObservacionMaquina         ob
        JOIN Maquina                    mq  ON ob.idMaquina = mq.idMaquina
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.Notificada = 0
        AND DATEPART(WEEK, ob.fecha) = @SemanaActual;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;


CREATE OR ALTER PROCEDURE spConsultaObservacionesHistoricas
    AS

    BEGIN TRY
        SELECT
            ob.idObservacionSector              AS idObservacion,
            ob.fecha                            AS fechaObservacion,
            ob.Descripcion                      AS descripcionObservacion,
            ob.estado                           AS estadoObservacion,
            ob.tipo                             AS tipoObservacion,
            ob.area                             AS areaObservacion,
            ob.horaInicio                       AS horaInicioObservacion,
            ob.HoraTermino                      AS horaTerminoObservacion,
            ob.minutosUsados                    AS duracionObservacion,
            ob.Notificada                       AS observacionNotificada,
            ob.comentarioEstado                 AS comentariosObservacion,
            'Sector'                            AS clasificacionObservacion,
            sp.idSector                         AS idEquipoSector,
            sp.nombreSector                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno

        FROM ObservacionSector          ob
        JOIN SectorPlanta               SP  ON ob.idSector = SP.idSector
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.Notificada = 0

        UNION ALL

        SELECT
            ob.idObservacionMaquina             AS idObservacion,
            ob.fecha                            AS fechaObservacion,
            ob.Descripcion                      AS descripcionObservacion,
            ob.estado                           AS estadoObservacion,
            ob.tipo                             AS tipoObservacion,
            ob.area                             AS areaObservacion,
            ob.horaInicio                       AS horaInicioObservacion,
            ob.HoraTermino                      AS horaTerminoObservacion,
            ob.minutosUsados                    AS duracionObservacion,
            ob.Notificada                       AS observacionNotificada,
            ob.comentarioEstado                 AS comentariosObservacion,
            'Maquina'                           AS clasificacionObservacion,
            mq.idMaquina                        AS idEquipoSector,
            mq.nombreEquipo                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno
        FROM ObservacionMaquina         ob
        JOIN Maquina                    mq  ON ob.idMaquina = mq.idMaquina
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.Notificada = 0
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spConsultaObservacionesFinalizadas
    AS

    BEGIN TRY
        SELECT
            ob.idObservacionSector              AS idObservacion,
            ob.fecha                            AS fechaObservacion,
            ob.Descripcion                      AS descripcionObservacion,
            ob.estado                           AS estadoObservacion,
            ob.tipo                             AS tipoObservacion,
            ob.area                             AS areaObservacion,
            ob.horaInicio                       AS horaInicioObservacion,
            ob.HoraTermino                      AS horaTerminoObservacion,
            ob.minutosUsados                    AS duracionObservacion,
            ob.Notificada                       AS observacionNotificada,
            ob.comentarioEstado                 AS comentariosObservacion,
            'Sector'                            AS clasificacionObservacion,
            sp.idSector                         AS idEquipoSector,
            sp.nombreSector                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno,
            fn.idUsuario                        AS idFinalizador,
            fn.nombreusuario                    AS nombreFinalizador
        FROM ObservacionSector          ob
        JOIN SectorPlanta               SP  ON ob.idSector = SP.idSector
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        JOIN Usuario                    Fn  ON fn.idUsuario = ob.FinalizadaPor
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.Notificada = 1

        UNION ALL

        SELECT
            ob.idObservacionMaquina             AS idObservacion,
            ob.fecha                            AS fechaObservacion,
            ob.Descripcion                      AS descripcionObservacion,
            ob.estado                           AS estadoObservacion,
            ob.tipo                             AS tipoObservacion,
            ob.area                             AS areaObservacion,
            ob.horaInicio                       AS horaInicioObservacion,
            ob.HoraTermino                      AS horaTerminoObservacion,
            ob.minutosUsados                    AS duracionObservacion,
            ob.Notificada                       AS observacionNotificada,
            ob.comentarioEstado                 AS comentariosObservacion,
            'Maquina'                           AS clasificacionObservacion,
            mq.idMaquina                        AS idEquipoSector,
            mq.nombreEquipo                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno,
            fn.idUsuario                        AS idFinalizador,
            fn.nombreusuario                    AS nombreFinalizador
        FROM ObservacionMaquina         ob
        JOIN Maquina                    mq  ON ob.idMaquina = mq.idMaquina
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        JOIN Usuario                    Fn  ON fn.idUsuario = ob.FinalizadaPor
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.Notificada = 1
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;


select * from ObservacionMaquina;









