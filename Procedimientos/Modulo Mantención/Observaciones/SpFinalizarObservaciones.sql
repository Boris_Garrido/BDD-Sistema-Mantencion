CREATE OR ALTER PROCEDURE spFinalizarObservacionSector
    @idObservacion       AS INT,
    @finalizarPor        AS INT,
    @comentarios         AS NVARCHAR(1000)
    AS

    BEGIN TRY

        DECLARE @respuesta  INT

        UPDATE ObservacionSector
        SET Notificada = 1,
            estado = 'Finalizada',
            ultimaActualizacion = GETDATE(),
            FinalizadaPor = @finalizarPor,
            comentarioEstado = @comentarios
        WHERE idObservacionSector = @idObservacion

        SET @respuesta = 1
        RETURN @respuesta;

    END TRY

    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta

    END CATCH
GO;

CREATE OR ALTER PROCEDURE spFinalizarObservacionMaquina
    @idObservacion       AS INT,
    @finalizarPor        AS INT,
    @comentarios         AS NVARCHAR(1000)
    AS

    BEGIN TRY

        DECLARE @respuesta  INT

        UPDATE ObservacionMaquina
        SET Notificada = 1,
            estado = 'Finalizada',
            ultimaActualizacion = GETDATE(),
            FinalizadaPor = @finalizarPor,
            comentarioEstado = @comentarios
        WHERE idObservacionMaquina = @idObservacion

        SET @respuesta = 1
        RETURN @respuesta;

    END TRY

    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta

    END CATCH
GO;

select * from ObservacionMaquina;