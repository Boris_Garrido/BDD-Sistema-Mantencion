CREATE OR ALTER PROCEDURE spAgregarObservacionMaquina

    @idUsuario                  INT,
    @idMaquina                  INT,
    @idTurno                    INT,
    @descripcionM               VARCHAR(600),
    @fecha                      DATETIME,
    @Estado                     VARCHAR(100),
    @Tipo                       VARCHAR(30),
    @area                       VARCHAR(50),
    @horaInicio                 TIME,
    @horaTermino                TIME,
    @idObservacion              INT OUTPUT
    AS
    BEGIN TRY

        DECLARE @Respuesta INT

        INSERT INTO ObservacionMaquina (idMaquina, idUsuario, idTurno, fecha, Descripcion, estado, Tipo, area, Notificada, horaInicio, HoraTermino, comentarioEstado)
        VALUES(
                                                    @idMaquina,
                                                    @idUsuario,
                                                    @idTurno,
                                                    @fecha,
                                                    @descripcionM,
                                                    @estado,
                                                    @Tipo,
                                                    @area,
                                                    0,
                                                    @horaInicio,
                                                    @horaTermino,
                                                    'Sin Comentarios'
                                                    )

        SET @Respuesta = 1

        SET @idObservacion = SCOPE_IDENTITY();
        RETURN @Respuesta
    END TRY
    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

















select * from ObservacionMaquina






