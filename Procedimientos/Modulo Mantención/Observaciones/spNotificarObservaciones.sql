CREATE OR ALTER PROCEDURE spNotificarObservaciones
    AS
    BEGIN TRY

        DECLARE @respuesta  INT

        IF EXISTS(

            SELECT 1 FROM ObservacionSector WHERE Notificada = 0
            UNION
            SELECT 1 FROM ObservacionMaquina WHERE Notificada = 0
            )
            BEGIN
               SET @respuesta = 2;
            END
        ELSE
            BEGIN
                SET @respuesta = 1
            END

        RETURN @respuesta;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;
        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;

delete from ObservacionSector
delete from ObservacionMaquina