CREATE OR ALTER PROCEDURE spModificarMantencionExterna
    @idMantencion           INT,
    @idequipo               INT,
    @empresaEncargada       VARCHAR(200),
    @descripcionBreve       NVARCHAR(250),
    @rutaArchivo            NVARCHAR(1000)
    AS
    BEGIN TRY

        DECLARE @respuesta      INT

        UPDATE MantencionExterna

        SET     idEquipo = @idEquipo,
                empresaEncargada = @empresaEncargada,
                fechaIngreso = GETDATE(),
                descripcionBreve = @descripcionBreve,
                rutaArchivoPdf = @rutaArchivo
        WHERE idMantencionExterna = @idMantencion

        SET @respuesta = 1
        RETURN @respuesta;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;