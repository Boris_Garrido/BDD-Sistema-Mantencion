CREATE OR ALTER PROCEDURE spAgregarMantencionExterna
    @idEquipo           INT,
    @empresaEncargada   VARCHAR(200),
    @descripcionBreve   NVARCHAR(250),
    @rutaArchivo        NVARCHAR(1000)
    AS
    BEGIN TRY

        DECLARE @respuesta INT

        INSERT INTO MantencionExterna VALUES (@idEquipo,
                                              @empresaEncargada,
                                              GETDATE(),
                                              @descripcionBreve,
                                              @rutaArchivo,
                                              1);

        SET @respuesta = 1
        RETURN @respuesta;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;
