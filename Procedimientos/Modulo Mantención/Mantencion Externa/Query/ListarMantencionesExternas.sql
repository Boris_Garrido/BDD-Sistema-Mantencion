CREATE OR ALTER PROCEDURE spListarMantencionesExternasPmaquina
    @idEquipo       INT,
    @estado         INT
    AS
    BEGIN TRY
        SELECT
            me.idMantencionExterna              AS idMantencion,
            me.descripcionBreve                 AS descripcionMantencion,
            me.rutaArchivoPdf                   AS rutaArchivoMantencion,
            me.fechaIngreso                     AS fechaActualizacionMantencion,
            me.empresaEncargada                 AS empresaEncargadaMantencion,
            me.habilitado                       AS estadoMantencion,
            maq.idMaquina                       AS idMaquina,
            maq.nombreEquipo                    AS nombreEquipo,
            maq.Foto                            AS fotoEquipo
        FROM MantencionExterna me
        JOIN Maquina maq ON maq.idMaquina = me.idEquipo
        WHERE me.idEquipo = @idEquipo AND me.habilitado = @estado
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;














