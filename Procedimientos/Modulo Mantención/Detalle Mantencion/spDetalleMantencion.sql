CREATE OR ALTER PROCEDURE spAgregarDetalleMantencionSector
    @idOperador         INT,
    @idMantencion       INT
    AS
    BEGIN TRY

        DECLARE @respuesta  INT

        INSERT INTO DetalleMantencionSector
        VALUES (@idOperador,
                @idMantencion)

        SET @respuesta = 1
        RETURN @respuesta;

    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spAgregarDetalleMantencionEquipo
    @idOperador         INT,
    @idMantencion       INT
    AS
    BEGIN TRY

        DECLARE @respuesta  INT

        INSERT INTO DetalleMantencionEquipo
        VALUES (@idOperador,
                @idMantencion)

        SET @respuesta = 1
        RETURN @respuesta;

    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;