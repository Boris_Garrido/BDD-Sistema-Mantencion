CREATE OR ALTER PROCEDURE spAgregarDetalleObservacionSector
    @idOperador         INT,
    @idMantencion       INT
    AS
    BEGIN TRY

        DECLARE @respuesta  INT

        INSERT INTO DetalleObservacionSector
        VALUES (@idOperador,
                @idMantencion)

        SET @respuesta = 1
        RETURN @respuesta;

    END TRY
    BEGIN CATCH
        SET @respuesta = 3
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        return @respuesta;
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spAgregarDetalleObservacionMaquina
    @idOperador         INT,
    @idMantencion       INT
    AS
    BEGIN TRY

        DECLARE @respuesta  INT

        INSERT INTO DetalleObservacionMaquina
        VALUES (@idOperador,
                @idMantencion)

        SET @respuesta = 1
        RETURN @respuesta;

    END TRY
    BEGIN CATCH
        SET @respuesta = 3
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        return @respuesta;
    END CATCH
GO;

DECLARE @idOperador INT = 1; -- Reemplaza con el valor adecuado
DECLARE @idMantencion INT = 18; -- Reemplaza con el valor adecuado

-- Declarar variable para capturar la respuesta
DECLARE @respuesta INT;

-- Ejecutar el procedimiento almacenado
EXEC @respuesta = spAgregarDetalleObservacionMaquina
    @idOperador,
    @idMantencion;

-- Verificar la respuesta
IF @respuesta = 1
BEGIN
    PRINT 'El procedimiento se ejecutó correctamente.';
END
ELSE
BEGIN
    PRINT 'Hubo un error al ejecutar el procedimiento.';
END

select * from errores;