CREATE OR ALTER PROCEDURE SpListarOtNoRealizadas
    AS
    BEGIN TRY
        SELECT
            OT.idOtMantencionMaquina            AS IdOt,
            OT.descripcionOt                    AS descripcionOt,
            OT.tipoOt                           AS tipoOt,
            OT.fechaSolicitud                   AS fechaSolicitud,
            OT.fechaRealizada                   AS fechaRealizada,
            OT.realizada                        AS realizada,
            'Mantencion Maquina'                AS clasificacionOT,
            T.idturno                           AS idTurno,
            T.nombreTurno                       AS nombreTurno,
            M.idMaquina                         AS idtipoEquipo,
            M.tipoMaquina                       AS nombreTipoEquipo,
            M.nombreEquipo                      AS nombreTipoRealizado,
            U.idUsuario                         AS idUsuario,
            U.nombreusuario                     AS nombreUsuario,
            R.idUsuario                         AS idRealizador,
            R.nombreusuario                     AS nombreRealizador
        FROM OtMantencionMaquina             OT
        LEFT JOIN Maquina                    M   ON OT.idMaquina = M.idMaquina
        LEFT JOIN Usuario                    U   ON OT.idSolicitante = U.idUsuario
        LEFT JOIN Usuario                    R   ON R.idUsuario = OT.idRealizador
        LEFT JOIN TURNO                      T   ON T.idturno = OT.idTurnoRealizado
        WHERE ot.realizada = 0

        UNION ALL

        SELECT
            OT.idOtMantencionSector             AS IdOt,
            OT.descripcionOt                    AS descripcionOt,
            OT.tipoOt                           AS tipoOt,
            OT.fechaSolicitud                   AS fechaSolicitud,
            OT.fechaRealizada                   AS fechaRealizada,
            OT.realizada                        AS realizada,
            'Mantencion Sector'                 AS clasificacionOT,
            T.idturno                           AS idTurno,
            T.nombreTurno                       AS nombreTurno,
            s.idSector                          AS idtipoEquipo,
            'No Aplica'                         AS nombreTipoEquipo,
            s.nombreSector                      AS nombreTipoRealizado,
            U.idUsuario                         AS idUsuario,
            U.nombreusuario                     AS nombreUsuario,
            R.idUsuario                         AS idRealizador,
            R.nombreusuario                     AS nombreRealizador
        FROM OtMantencionSector              OT
        LEFT JOIN SectorPlanta               S   ON OT.idSector = S.idSector
        LEFT JOIN Usuario                    U   ON OT.idSolicitante = U.idUsuario
        LEFT JOIN Usuario                    R   ON R.idUsuario = OT.idRealizador
        LEFT JOIN TURNO                      T   ON T.idturno = OT.idTurnoRealizado
        WHERE ot.realizada = 0

        UNION ALL

        SELECT
            OT.idOtMantencionSubSistema         AS IdOt,
            OT.descripcionOt                    AS descripcionOt,
            OT.tipoOt                           AS tipoOt,
            OT.fechaSolicitud                   AS fechaSolicitud,
            OT.fechaRealizada                   AS fechaRealizada,
            OT.realizada                        AS realizada,
            'Mantencion SubSistema'             AS clasificacionOT,
            T.idturno                           AS idTurno,
            T.nombreTurno                       AS nombreTurno,
            SS.idSubSistema                     AS idtipoEquipo,
            M.tipoMaquina                       AS nombreTipoEquipo,
            SS.nombreSubSistema                 AS nombreTipoRealizado,
            U.idUsuario                         AS idUsuario,
            U.nombreusuario                     AS nombreUsuario,
            R.idUsuario                         AS idRealizador,
            R.nombreusuario                     AS nombreRealizador
        FROM OtMantencionSubSistema          OT
        LEFT JOIN Subsistema                 SS  ON OT.idSubsistema = SS.idSubSistema
        LEFT JOIN Usuario                    U   ON OT.idSolicitante = U.idUsuario
        LEFT JOIN Usuario                    R   ON R.idUsuario = OT.idRealizador
        LEFT JOIN TURNO                      T   ON T.idturno = OT.idTurnoRealizado
        LEFT JOIN Maquina                    M   ON SS.idMaquina = M.idMaquina
        WHERE ot.realizada = 0
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;
        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;



delete from OtMantencionSubSistema
delete from OtMantencionSector
delete from OtMantencionMaquina


























