CREATE OR ALTER PROCEDURE spAgregarOtMantencionMaquina
    @idMaquina              INT,
    @idUsuario              INT,
    @descripcionOT          NVARCHAR(1000),
    @tipoOt                 NVARCHAR(100),
    @idCreada               INT OUTPUT
    AS

    BEGIN TRY
        DECLARE @respuesta      INT


        INSERT INTO OtMantencionMaquina VALUES(@idMaquina,
                                               @idUsuario,
                                               null,
                                               @descripcionOT,
                                               @tipoOt,
                                               GETDATE(),
                                               null,
                                               null,
                                               0)


        SET @respuesta = 1;
        SET @idCreada = (SELECT SCOPE_IDENTITY())

        RETURN @respuesta

    END TRY

    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spFinalizarOtMaquina
    @idOt                           INT,
    @idRealizador                   INT,
    @idTurno                        INT
    AS

    BEGIN TRY
        DECLARE @respuesta      INT


        UPDATE OtMantencionMaquina
        SET     idRealizador        = @idRealizador,
                fechaRealizada      = GETDATE(),
                idTurnoRealizado    = @idTurno,
                realizada           = 1
        WHERE   idOtMantencionMaquina = @idOt
        SET @respuesta = 1;
        RETURN @respuesta

    END TRY

    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;




SELECT
    o.idOrdenTrabajo                        AS idOrdenTrabajo,
    o.Descripcion                           AS descripcionOt,
    o.fecha                                 AS fechaRealizada,
    o.estado                                AS estado,
    o.tipoOt                                AS tipoOt,
    s.idSubSistema                          AS idSubsistema,
    s.nombreSubSistema                      AS nombreSubSistema,
    s.voltaje                               AS voltajeSubsistema,
    s.potencia                              AS potenciaSubSistema,
    s.fases                                 AS fasesSubsistema,
    s.amperaje                              AS amperajeSs,
    s.Frecuencia                            AS frecuenciaSs,
    s.consumoAgua                           AS consumoAguaSs,
    m.idMaquina                             AS idMaquina,
    m.nombreEquipo                          AS nombreEquipo,
    sp.idSector                             AS idSector,
    sp.nombreSector                         As nombreSector,
    u.idUsuario                             AS idUsuario,
    u.nombreusuario                         AS nombreusuario,
    u.tipoUsuario                           AS tipoUsuario,
    t.idturno                               AS idTurno,
    t.nombreTurno                           AS nombreTurno,
    t.nombreAlt                             AS nombreTurnoAlt
from OrdenTrabajo       O
JOIN Subsistema         S  ON o.idSubSistema = S.idSubSistema
JOIN Usuario            U  ON o.idusuario = U.idUsuario
JOIN turno              T  ON t.idturno = o.idTurno
JOIN Maquina            M  ON m.idMaquina = s.idMaquina
JOIN SectorPlanta       sp ON sp.idSector = m.idSector
WHERE m.idMaquina = 1;