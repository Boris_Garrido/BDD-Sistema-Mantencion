CREATE OR ALTER PROCEDURE spAgregarOtSubSistema
    @idSubsistema           INT,
    @idUsuario              INT,
    @descripcionOT          NVARCHAR(1000),
    @tipoOt                 NVARCHAR(100)
    AS

    BEGIN TRY
        DECLARE @respuesta      INT


        INSERT INTO OtMantencionSubSistema      VALUES( @idSubsistema,
                                                        @idUsuario,
                                                        null,
                                                        @descripcionOT,
                                                        @tipoOt,
                                                        GETDATE(),
                                                        null,
                                                        null,
                                                        0)

        SET @respuesta = 1;
        RETURN @respuesta

    END TRY

    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spFinalizarOtSubSistema
    @idOt                           INT,
    @idRealizador                   INT,
    @idTurno                        INT
    AS

    BEGIN TRY
        DECLARE @respuesta      INT


        UPDATE OtMantencionSubSistema
        SET     idRealizador        = @idRealizador,
                fechaRealizada      = GETDATE(),
                idTurnoRealizado    = @idTurno,
                realizada           = 1
        WHERE   idOtMantencionSubSistema = @idOt
        SET @respuesta = 1;
        RETURN @respuesta

    END TRY

    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

select * from OtMantencionSubSistema;