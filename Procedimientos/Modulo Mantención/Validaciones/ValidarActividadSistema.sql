CREATE OR ALTER PROCEDURE spValidarActividadDiaria
    @idActividad        INT,
    @idMaquina          INT
    AS

    BEGIN TRY

        SET NOCOUNT ON;
        DECLARE @rowCount       INT
        DECLARE @respuesta      INT

        SELECT
            *
        FROM BitacoraDiaria                 b
        INNER JOIN Maquina                  M     ON m.idMaquina = b.idMaquina
        INNER JOIN MantencionGeneralMaquina mm    ON b.descripcion = mm.Descripcion AND b.idMaquina = mm.idMaquina
        WHERE CAST(mm.fecha AS DATE) = CAST(GETDATE() AS DATE)
        AND b.idBitacora = @idActividad
        AND m.idMaquina = @idMaquina

        SET @rowCount = @@rowcount

        IF(@rowCount > 0)
            BEGIN
                SET @respuesta = 2;
                PRINT @respuesta;
                RETURN @respuesta
            END
        ELSE
            BEGIN
                SET @respuesta = 1;
                PRINT @respuesta;
                RETURN @respuesta

            END
    END TRY
    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();


        PRINT ERROR_MESSAGE()
        RETURN @respuesta

    END CATCH
GO;




DECLARE @Valor1 INT, @Valor2 INT

SET @Valor1 = 1
SET @Valor2 = 1

EXEC spValidarActividadDiaria @idActividad = 4, @idMaquina = 2








insert into ActividadesGenericasSistema values (1,'Inspeccion Mecanica')




select * from ActividadesGenericasSistema;

        SELECT
            *
        FROM ActividadesGenericasSistema a
        INNER JOIN Subsistema S                   ON a.idSubSistema = S.idSubSistema
        INNER JOIN OrdenTrabajo o                 ON a.descripcion = o.Descripcion
        WHERE CAST(o.fecha AS DATE) = CAST(GETDATE() AS DATE)
        AND a.idActividadSistema = 3


select * from OrdenTrabajo;