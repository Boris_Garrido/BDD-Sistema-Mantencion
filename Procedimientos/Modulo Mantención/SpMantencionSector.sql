CREATE OR ALTER PROCEDURE spAgregarMantencionSector

    @idusuario             INT,
    @idSector               INT,
    @idTurno                INT,
    @descripcionM           VARCHAR(600),
    @fecha                  DATETIME,
    @Estado                 VARCHAR(100),
    @Tipo                   VARCHAR(30),
    @area                   VARCHAR(50),
    @horaInicio             TIME,
    @horaTermino            TIME,
    @idMantencion           INT OUTPUT
    AS
    BEGIN TRY

        DECLARE @Respuesta INT

        INSERT INTO MantencionSector (idsector, idusuario, idturno, fecha, descripcion, estado, tipo, area, horainicio, horatermino)
                                        VALUES(
                                        @idSector,
                                        @idusuario,
                                        @idTurno,
                                        @fecha,
                                        @descripcionM,
                                        @estado,
                                        @Tipo,
                                        @area,
                                        @horaInicio,
                                        @horaTermino
                                       )

        SET @Respuesta = 1
        SET @idMantencion = SCOPE_IDENTITY();
        RETURN @Respuesta
    END TRY
    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spAgregarMantencionSectorSinHora

    @idusuario             INT,
    @idSector               INT,
    @idTurno                INT,
    @descripcionM           VARCHAR(600),
    @fecha                  DATETIME,
    @Estado                 VARCHAR(100),
    @Tipo                   VARCHAR(30),
    @area                   VARCHAR(50),
    @idMantencion           INT OUTPUT
    AS
    BEGIN TRY

        DECLARE @Respuesta INT

        INSERT INTO MantencionSector (idsector, idusuario, idturno, fecha, descripcion, estado, tipo, area, horainicio, horatermino)
                                        VALUES(
                                        @idSector,
                                        @idusuario,
                                        @idTurno,
                                        @fecha,
                                        @descripcionM,
                                        @estado,
                                        @Tipo,
                                        @area,
                                        null,
                                        null
                                       )

        SET @Respuesta = 1
        SET @idMantencion = SCOPE_IDENTITY();
        RETURN @Respuesta
    END TRY
    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

