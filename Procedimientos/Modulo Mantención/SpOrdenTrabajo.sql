CREATE OR ALTER PROCEDURE spAgregarOrdenTrabajo
    @idSubSistema           INT,
    @idusuario              INT,
    @idturno                INT,
    @fecha                  DATETIME,
    @descripcionOt          VARCHAR(1000),
    @estado                 VARCHAR(200),
    @tipoOt                 VARCHAR(200),
    @area                   VARCHAR(50)
    AS
    BEGIN TRY

        DECLARE @Respuesta INT

        INSERT INTO OrdenTrabajo VALUES(
                                        @idSubSistema,
                                        @idusuario,
                                        @idturno,
                                        @fecha,
                                        @descripcionOt,
                                        @estado,
                                        @tipoOt,
                                        @area
                                       )

        SET @Respuesta = 1
        RETURN @Respuesta
    END TRY
    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

