CREATE OR ALTER   PROCEDURE spReporteTurno
    @idTurno            INT
    AS
    BEGIN TRY

        SELECT
            ob.idObservacionSector              AS idActividad,
            ob.fecha                            AS fechaActividad,
            ob.Descripcion                      AS descripcionActividad,
            ob.estado                           AS estadoActividad,
            ob.tipo                             AS tipoActividad,
            ob.area                             AS areaActividad,
            ob.horaInicio                       AS horaInicioActividad,
            ob.HoraTermino                      AS horaTerminoActividad,
            ob.minutosUsados                    AS duracionActividad,
            ob.Notificada                       AS actividadNotificada,
            'Observacion Sector'                AS clasificacionActividad,
            sp.idSector                         AS idEquipoSector,
            sp.nombreSector                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno,
            fn.idUsuario                        AS idFinalizador,
            fn.nombreusuario                    AS nombreFinalizador
        FROM ObservacionSector          ob
        JOIN SectorPlanta               SP  ON ob.idSector = SP.idSector
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        LEFT JOIN Usuario               Fn  ON fn.idUsuario = ob.FinalizadaPor
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.idTurno = @idTurno
        AND CAST(ob.fecha AS DATE) = CAST(GETDATE() AS DATE)


        UNION ALL

        SELECT
            ob.idObservacionMaquina             AS idActividad,
            ob.fecha                            AS fechaActividad,
            ob.Descripcion                      AS descripcionActividad,
            ob.estado                           AS estadoActividad,
            ob.tipo                             AS tipoActividad,
            ob.area                             AS areaActividad,
            ob.horaInicio                       AS horaInicioActividad,
            ob.HoraTermino                      AS horaTerminoActividad,
            ob.minutosUsados                    AS duracionActividad,
            ob.Notificada                       AS actividadNotificada,
            'Observacion Maquina'               AS clasificacionActividad,
            mq.idMaquina                        AS idEquipoSector,
            mq.nombreEquipo                     AS nombreEquipoSector,
            u.idUsuario                         AS idRealizador,
            u.nombreusuario                     AS nombreRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno,
            fn.idUsuario                        AS idFinalizador,
            fn.nombreusuario                    AS nombreFinalizador
        FROM ObservacionMaquina         ob
        JOIN Maquina                    mq  ON ob.idMaquina = mq.idMaquina
        JOIN Usuario                    U   ON ob.idUsuario = U.idUsuario
        JOIN Usuario                    Fn  ON fn.idUsuario = ob.FinalizadaPor
        JOIN turno                      T   ON t.idturno = ob.idTurno
        LEFT JOIN Usuario               F   ON f.idUsuario = ob.FinalizadaPor
        WHERE ob.idTurno = @idTurno
        AND CAST(ob.fecha AS DATE) = CAST(GETDATE() AS DATE)



        UNION ALL

        SELECT
            mm.idMantencionGeneral              AS idActividad,
            mm.fecha                            AS fechaActividad,
            mm.Descripcion                      AS descripcionActividad,
            mm.estado                           AS estadoActividad,
            mm.Tipo                             AS tipoActividad,
            mm.area                             AS areaActividad,
            mm.horaInicio                       AS horaInicioActividad,
            mm.HoraTermino                      AS horaTerminoActividad,
            mm.minutosUsados                    AS duracionActividad,
            0                                   AS actividadNotificada,
            'Mantencion Maquina'                AS clasificacionActividad,
            maq.idMaquina                       AS idEquipoSector,
            maq.nombreEquipo                    AS nombreEquipoSector,
            u.idUsuario                         AS idUsuarioRealizador,
            u.nombreusuario                     AS nombreUsuarioRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno,
            null                                AS idFinalizador,
            null                                AS nombreFinalizador


        FROM MantencionGeneralMaquina   mm
        JOIN Maquina                    maq ON maq.idMaquina = mm.idMaquina
        JOIN Usuario                    U   ON U.idUsuario = mm.idUsuario
        JOIN turno                      T   ON t.idturno = mm.idTurno
        WHERE mm.idTurno = @idTurno
        AND CAST(mm.fecha AS DATE) = CAST(GETDATE() AS DATE)


        UNION ALL

        SELECT
            ms.idMantencionSector               AS idActividad,
            ms.fecha                            AS fechaActividad,
            ms.Descripcion                      AS descripcionActividad,
            ms.estado                           AS estadoActividad,
            ms.Tipo                             AS tipoActividad,
            ms.area                             AS areaActividad,
            ms.horaInicio                       AS horaInicioActividad,
            ms.HoraTermino                      AS horaTerminoActividad,
            ms.minutosUsados                    AS duracionActividad,
            0                                   AS actividadNotificada,
            'Mantencion Sector'                 AS clasificacionActividad,
            s.idSector                          AS idEquipoSector,
            s.nombreSector                      AS nombreEquipoSector,
            u.idUsuario                         AS idUsuarioRealizador,
            u.nombreusuario                     AS nombreUsuarioRealizador,
            u.tipoUsuario                       AS tipoUsuario,
            t.idturno                           AS idTurno,
            t.nombreTurno                       AS nombreTurno,
            null                                AS idFinalizador,
            null                                AS nombreFinalizador


        FROM MantencionSector           ms
        JOIN SectorPlanta               s   ON s.idSector = ms.idSector
        JOIN Usuario                    U   ON U.idUsuario = ms.idUsuario
        JOIN turno                      T   ON t.idturno = ms.idTurno
        WHERE ms.idTurno = @idTurno
        AND CAST(ms.fecha AS DATE) = CAST(GETDATE() AS DATE)

    end try
    begin catch
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    end catch
go

EXEC spReporteTurno @idTurno = 1;


select * from turno;









