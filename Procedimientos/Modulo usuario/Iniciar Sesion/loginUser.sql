CREATE OR ALTER PROCEDURE spLogearUsuario
    @nombreUsuario          NVARCHAR(255),
    @password               NVARCHAR(50),
    @tipoUsuario            NVARCHAR(50) OUTPUT,
    @idUsuario              INT OUTPUT
    AS
    BEGIN TRY
        DECLARE @respuesta      INT

        IF EXISTS(SELECT idusuario FROM Usuario WHERE nombreusuario = @nombreUsuario)
            BEGIN
                SET @idUsuario=(SELECT idUsuario FROM Usuario WHERE nombreusuario=@nombreUsuario AND contrasenia=HASHBYTES('SHA2_512', @password+CAST(identificador AS NVARCHAR(36))))
                IF(@idUsuario IS NULL)
                    BEGIN
                        SET @respuesta = 2;
                        SET @tipoUsuario = ''
                        RETURN @respuesta
                    END
                ELSE
                    BEGIN
                        DECLARE @habilitado BIT

                        SET @habilitado = (SELECT habilitado FROM Usuario WHERE idUsuario = @idUsuario)
                        IF(@habilitado = 1)
                            BEGIN
                                SET @respuesta = 1;
                                SET @tipoUsuario = (SELECT tipoUsuario FROM Usuario WHERE idUsuario = @idUsuario)
                                RETURN @respuesta
                            END
                        ELSE
                            BEGIN
                                SET @respuesta = 5
                                SET @tipoUsuario = ''
                                RETURN @respuesta
                            END
                    END
            END
        ELSE
            BEGIN
                SET @respuesta = 4
                SET @tipoUsuario = ''
                RETURN @respuesta
            END
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;
        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;


DECLARE @nombreUsuario NVARCHAR(255) = 'test';
DECLARE @password NVARCHAR(50) = '2450';
DECLARE @tipoUsuario NVARCHAR(50);
DECLARE @idUsuario INT;
DECLARE @respuesta INT;

EXEC @respuesta = spLogearUsuario @nombreUsuario, @password, @tipoUsuario OUTPUT, @idUsuario OUTPUT;

IF @respuesta = 1
BEGIN
    SELECT @tipoUsuario AS 'TipoUsuario';
END

ELSE IF @respuesta = 2
    BEGIN
        PRINT   'Contrasenia mala'
    end

ELSE IF @respuesta = 3
    BEGIN
        PRINT   'Error en la bdd'
    end

ELSE IF @respuesta = 4
    BEGIN
        PRINT   'Nombre de usuario no existe'
    end

ELSE IF @respuesta = 5
    BEGIN
        PRINT   'Usuario no habilitado'
    end


select * from Usuario;

