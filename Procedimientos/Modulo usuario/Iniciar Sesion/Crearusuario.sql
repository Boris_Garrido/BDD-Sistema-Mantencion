CREATE OR ALTER PROCEDURE spAgregarusuario
    @usuario            NVARCHAR(50),
    @contrasenia        NVARCHAR(50),
    @tipoUsuario        NVARCHAR(40) = NULL
    AS
    BEGIN TRY

        SET NOCOUNT ON
        DECLARE @salt UNIQUEIDENTIFIER=NEWID()
        DECLARE @respuesta  INT

        INSERT INTO Usuario (nombreusuario, contrasenia, identificador, tipoUsuario,habilitado)
        VALUES(@usuario, HASHBYTES('SHA2_512', @contrasenia+CAST(@salt AS NVARCHAR(36))), @salt, @tipoUsuario,1)

        SET @respuesta = 1
        RETURN @respuesta;

    END TRY
    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro

    END CATCH

GO;

DECLARE @usuario NVARCHAR(50) = 'Frigorista2'; -- Reemplaza 'usuarioNuevo' con el nombre de usuario que deseas agregar
DECLARE @contrasenia NVARCHAR(50) = '2450'; -- Reemplaza 'passwordNuevo' con la contraseña que deseas establecer
DECLARE @tipoUsuario NVARCHAR(40) = 'Frigorista'; -- Reemplaza 'TipoNuevo' con el tipo de usuario que deseas asignar (opcional)

EXEC spAgregarusuario @usuario, @contrasenia, @tipoUsuario;

select * from Usuario;

delete from Usuario
where idUsuario = 1008