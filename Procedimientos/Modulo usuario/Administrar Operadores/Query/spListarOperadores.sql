CREATE OR ALTER PROCEDURE spListarOperadores
    @estado         INT
    AS
    BEGIN TRY
        SELECT
            idOperador              AS idOperador,
            nombreOperador          AS nombreOperador,
            tipo                    AS tipoOperador,
            habilitado              AS habilitado
        FROM Operador
        WHERE habilitado = @estado;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;





















