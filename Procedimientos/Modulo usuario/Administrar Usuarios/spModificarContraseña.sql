CREATE OR ALTER PROCEDURE spModificarPassword
    @idUsuario      INT,
    @nuevopass      NVARCHAR(100)
    AS
    BEGIN TRY

        DECLARE @respuesta      INT
        DECLARE @salt           UNIQUEIDENTIFIER
        SET @salt = (SELECT identificador FROM Usuario where idUsuario = @idUsuario)

        UPDATE Usuario
        SET contrasenia =  HASHBYTES('SHA2_512', @nuevopass+CAST(@salt AS NVARCHAR(36)))
        where idUsuario = @idUsuario

        SET @respuesta = 1;
        RETURN @respuesta;
    end try
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro

        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

select * from Usuario;


DECLARE @respuesta INT;
EXEC @respuesta = spModificarPassword @idUsuario = 1, @nuevopass = '2450';
PRINT 'Respuesta: ' + CAST(@respuesta AS VARCHAR(10));