CREATE OR ALTER PROCEDURE spDeshabilitarUsuario
    @idUsuario          INT
    AS
    BEGIN TRY

        DECLARE @respuesta      INT

        UPDATE Usuario
        SET habilitado = 0
        WHERE idUsuario = @idUsuario;

        SET @respuesta = 1
        RETURN @respuesta;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro

        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spHabilitarUsuario
    @idUsuario          INT
    AS
    BEGIN TRY

        DECLARE @respuesta      INT

        UPDATE Usuario
        SET habilitado = 1
        WHERE idUsuario = @idUsuario;

        SET @respuesta = 1
        RETURN @respuesta;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro

        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

DECLARE @respuesta INT;
EXEC @respuesta = spHabilitarUsuario @idUsuario = 1;
PRINT 'Respuesta: ' + CAST(@respuesta AS VARCHAR(10));