CREATE OR ALTER PROCEDURE spListarManuales
    AS
    BEGIN TRY

        SELECT

            mm.idManual                 AS idManual,
            mm.fechaCreacion            AS fechaCreacionManual,
            mm.descripcionManual        AS descripcionManual,
            mm.rutaArchivo              AS rutaArchivo,
            mm.habilitado               AS habilitado,
            m.idMaquina                 AS idMaquina,
            m.nombreEquipo              AS nombreEquipo,
            m.Foto                      AS fotoEquipo
        FROM    ManualMaquinas          mm
        JOIN    Maquina                m ON m.idMaquina = mm.idMaquina
        WHERE mm.habilitado = 1
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spListarManualesDeshabilitados
    AS
    BEGIN TRY

        SELECT

            mm.idManual                 AS idManual,
            mm.fechaCreacion            AS fechaCreacionManual,
            mm.descripcionManual        AS descripcionManual,
            mm.rutaArchivo              AS rutaArchivo,
            mm.habilitado               AS habilitado,
            m.idMaquina                 AS idMaquina,
            m.nombreEquipo              AS nombreEquipo,
            m.Foto                      AS fotoEquipo
        FROM    ManualMaquinas          mm
        JOIN    Maquina                m ON m.idMaquina = mm.idMaquina
        WHERE mm.habilitado = 0
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;


select * from planMantencionAnual;

update ManualMaquinas
set habilitado = 0
where idManual > 40