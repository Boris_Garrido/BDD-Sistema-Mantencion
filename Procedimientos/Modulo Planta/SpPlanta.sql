CREATE OR ALTER PROCEDURE spAgregarPlanta
    @nombrePlanta       VARCHAR(100),
    @direccionPlanta    VARCHAR(200),
    @telefonoPlanta     VARCHAR(30)
    AS
    BEGIN TRY
        DECLARE @respuesta INT

        INSERT INTO Planta VALUES (@nombrePlanta,@direccionPlanta,@telefonoPlanta,1)

        SET @respuesta = 1;
        RETURN @respuesta
    END TRY

    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spEditarPlanta
    @idPlanta           INT,
    @nombrePlanta       VARCHAR(100),
    @direccionPlanta    VARCHAR(200),
    @telefonoPlanta     VARCHAR(30)
    AS
    BEGIN TRY
        DECLARE @respuesta INT

        UPDATE Planta
        SET nombrePlanta = @nombrePlanta,
            direccionPlanta = @direccionPlanta,
            telefonoPlanta = @telefonoPlanta
        WHERE idPlanta = @idPlanta

        SET @respuesta = 1;
        RETURN @respuesta
    END TRY

    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro

    END CATCH
GO;

CREATE OR ALTER PROCEDURE spHabilitacionPlanta
    @idPlanta           INT,
    @habilitar          INT
    AS
    BEGIN TRY
        DECLARE @respuesta INT

        UPDATE Planta
        SET habilitad = @habilitar
        WHERE idPlanta = @idPlanta

        SET @respuesta = 1;
        RETURN @respuesta
    END TRY

    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro

    END CATCH
GO;