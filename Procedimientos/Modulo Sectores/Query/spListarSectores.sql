CREATE OR ALTER PROCEDURE spListarSectoresHabilitados
    AS
    BEGIN TRY
        SELECT
            s.idSector              AS idSector,
            s.nombreSector          AS nombreSector,
            s.Habilitado            AS habilitado,
            p.idPlanta              AS idPlanta,
            p.nombrePlanta          AS nombrePlanta
        FROM SectorPlanta   s
        JOIN Planta         p   ON p.idPlanta = s.idPlanta
        WHERE s.habilitado = 1;
    END TRY
    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spListarSectoresDeshabilitados
    AS
    BEGIN TRY
        SELECT
            s.idSector              AS idSector,
            s.nombreSector          AS nombreSector,
            s.Habilitado            AS habilitado,
            p.idPlanta              AS idPlanta,
            p.nombrePlanta          AS nombrePlanta
        FROM SectorPlanta   s
        JOIN Planta         p   ON p.idPlanta = s.idPlanta
        WHERE s.habilitado = 0;
    END TRY
    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;



