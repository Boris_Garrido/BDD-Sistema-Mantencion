CREATE OR ALTER PROCEDURE spModificarSector
    @idPlanta           INT,
    @idSector           INT,
    @nombreSector       NVARCHAR(200)
    AS
    BEGIN TRY

        DECLARE @respuesta  INT

        UPDATE SectorPlanta
        SET nombreSector = @nombreSector,
            idPlanta    = @idPlanta
        WHERE idSector = @idSector


        SET @respuesta = 1
        RETURN @respuesta;
    END TRY
    BEGIN CATCH
        SET @respuesta = 3
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        RETURN @respuesta;
    END CATCH
GO;