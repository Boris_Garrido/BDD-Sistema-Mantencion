CREATE OR ALTER PROCEDURE spHabilitarSector
    @idSector           INT
    AS
    BEGIN TRY

        DECLARE @respuesta  INT

        UPDATE SectorPlanta
        SET Habilitado = 1
        WHERE idSector = @idSector


        SET @respuesta = 1
        RETURN @respuesta;
    END TRY
    BEGIN CATCH
        SET @respuesta = 3
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        RETURN @respuesta;
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spDeshabilitarSector
    @idSector           INT
    AS
    BEGIN TRY

        DECLARE @respuesta  INT

        UPDATE SectorPlanta
        SET Habilitado = 0
        WHERE idSector = @idSector


        SET @respuesta = 1
        RETURN @respuesta;
    END TRY
    BEGIN CATCH
        SET @respuesta = 3
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        RETURN @respuesta;
    END CATCH
GO;

USER_id

INSERT INTO turno values ('Turno 3', 'TEC')

