CREATE OR ALTER PROCEDURE spFinalizarActividadPlanAnual

    @idUsuario                  INT,
    @idMaquina                  INT,
    @idTurno                    INT,
    @descripcionM               VARCHAR(600),
    @fecha                      DATETIME,
    @Estado                     VARCHAR(100),
    @area                       VARCHAR(50)
    AS
    BEGIN TRY

        DECLARE @Respuesta INT

        INSERT INTO MantencionGeneralMaquina(idmaquina, idusuario, idturno, fecha, descripcion, estado, tipo, area, horainicio, horatermino)

                                                    VALUES(
                                                    @idMaquina,
                                                    @idUsuario,
                                                    @idTurno,
                                                    @fecha,
                                                    @descripcionM,
                                                    @estado,
                                                    'Plan de mantencion Anual',
                                                    @area,
                                                    null,
                                                    null
                                                    )

        SET @Respuesta = 1
        RETURN @Respuesta
    END TRY
    BEGIN CATCH

        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();

        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;