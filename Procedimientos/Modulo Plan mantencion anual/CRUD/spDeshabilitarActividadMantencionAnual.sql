CREATE OR ALTER PROCEDURE spCambiarEstadoMantencionAnual
    @idMantencion       INT,
    @estado             INT
    AS

    BEGIN TRY

        DECLARE @respuesta      INT

        UPDATE planMantencionAnual
        SET habilitado = @estado
        WHERE idMantencion = @idMantencion

        SET @respuesta = 1
        RETURN @respuesta;

    END TRY
    BEGIN CATCH
        SET @respuesta = 3
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        RETURN @respuesta;
    END CATCH
GO;