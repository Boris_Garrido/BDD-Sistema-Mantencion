CREATE OR ALTER PROCEDURE spAgregarMantencionAnual
    @descripcionMantencion      AS NVARCHAR(1000),
    @areaMantencion             AS NVARCHAR(50),
    @idEquipo                   AS INT
    AS
    BEGIN TRY

        DECLARE @respuesta INT

        INSERT INTO planMantencionAnual(idEquipo,descripcionMantencion,fechaCreacion,areaMantencion,tipoActividad,fechaActualizacion,habilitado)

        VALUES (@idEquipo,@descripcionMantencion,GETDATE(),@areaMantencion,'Plan de mantencion Anual', GETDATE(),1)

        SET @respuesta = 1

        RETURN @respuesta;

    END TRY

    BEGIN CATCH
        SET @respuesta = 3
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        RETURN @respuesta;
    END CATCH
GO;

select * from planMantencionAnual;