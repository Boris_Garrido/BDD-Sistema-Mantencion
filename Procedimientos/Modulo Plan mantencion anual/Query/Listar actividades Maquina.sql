CREATE OR ALTER PROCEDURE spListarPlanMaquina
    @idMaquina       INT
    AS
    BEGIN TRY
        SELECT
            p.idEquipo                  AS IdEquipo,
            p.descripcionMantencion     AS descripcionActividad,
            p.areaMantencion            AS areaMantencion,
            p.fechaCreacion             AS fechaCreacionActividad,
            CASE
                WHEN mm.idMantencionGeneral IS NULL THEN 0
                ELSE 1
            END                         AS actividadRealizada
        FROM planMantencionAnual p
        LEFT JOIN Maquina m                      ON m.idMaquina       = p.idEquipo
        LEFT JOIN MantencionGeneralMaquina mm    ON mm.Descripcion = p.descripcionMantencion AND p.idEquipo = mm.idMaquina AND YEAR(mm.fecha) = YEAR(GETDATE())
        WHERE p.idEquipo = @idmaquina
        AND p.habilitado = 1;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;

        SELECT
            p.idEquipo                  AS IdEquipo,
            p.descripcionMantencion     AS descripcionActividad,
            p.areaMantencion            AS areaMantencion,
            p.fechaCreacion             AS fechaCreacionActividad,
            CASE
                WHEN mm.idMantencionGeneral IS NULL THEN 0
                ELSE 1
            END                         AS actividadRealizada
        FROM planMantencionAnual p
        LEFT JOIN Maquina m                      ON m.idMaquina       = p.idEquipo
        LEFT JOIN MantencionGeneralMaquina mm    ON mm.Descripcion = p.descripcionMantencion AND p.idEquipo = mm.idMaquina
        WHERE p.idEquipo = 1
        AND habilitado = 1