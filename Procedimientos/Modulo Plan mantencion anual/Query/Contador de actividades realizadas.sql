CREATE OR ALTER  PROCEDURE spListarEquiposMantencionAnual
    AS
    BEGIN TRY
        SELECT
            p.idEquipo                                                                      AS IdEquipo,
            m.nombreEquipo                                                                  AS nombreEquipo,
            m.Foto                                                                          AS foto,
            SUM(IIF(mm.idMantencionGeneral IS NULL, 0, 1)) AS actividadRealizada,
            (Select COUNT(*) FROM planMantencionAnual  pt WHERE pt.idEquipo = p.idEquipo and pt.habilitado = 1 )  AS totalActividades
        FROM planMantencionAnual p
        LEFT JOIN Maquina m                      ON m.idMaquina       = p.idEquipo
        LEFT JOIN MantencionGeneralMaquina mm    ON mm.Descripcion = p.descripcionMantencion AND p.idEquipo = mm.idMaquina
        where p.habilitado = 1
        GROUP BY p.idEquipo, m.nombreEquipo,m.Foto;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO

select * from MantencionGeneralMaquina
order by idMantencionGeneral desc;

delete from  MantencionGeneralMaquina
where idMantencionGeneral between 25 and 40



select * from DetalleMantencionEquipo d
join Usuario u on d.idOperador = u.idUsuario


SELECT
                                m.idDetalleMantencionEquipo     AS idDetalleMantencion,
                                o.nombreOperador                AS nombreOperador,
                                m.idMantencionEquipo            AS idMantencion
                            FROM DetalleMantencionEquipo m
                            JOIN Operador O on m.idOperador = O.idOperador

update MantencionGeneralMaquina
set Tipo = 'Novedad'


       SELECT
            idMantencionGeneral         AS idMantencion,
            Descripcion                 AS descripcion,
            'maquina'                   AS tipo,
            'Novedad'                   AS clasificacion,
            mm.area                     AS area,
            m.nombreEquipo              AS realizadaA,
            t.nombreTurno               AS turno,
            mm.fecha                    AS fecha,
            us.nombreusuario            AS nombreusuario,
            mm.horaInicio               AS horaInicio,
            mm.HoraTermino              AS horaTermino,
            mm.minutosUsados            AS DuracionMinutos

        FROM MantencionGeneralMaquina   mm
        JOIN Maquina                    m   ON mm.idMaquina = m.idMaquina
        JOIN turno                      t   ON mm.idTurno = t.idturno
        JOIN Usuario                    Us  ON us.idUsuario = mm.idUsuario
        WHERE mm.Tipo LIKE 'Novedad'
        AND   CAST(mm.fecha AS DATE) = CAST(GETDATE() AS DATE)