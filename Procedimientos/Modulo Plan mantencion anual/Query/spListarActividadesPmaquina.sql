CREATE OR ALTER PROCEDURE spListarPlanMantencionPmaquina
    @idEquipo       INT
    AS

    BEGIN TRY
        SELECT
            idMantencion                        AS idMantencion,
            fechaCreacion                       AS fechaCreacion,
            descripcionMantencion               AS descripcionMantencion,
            areaMantencion                      AS areaMantencion,
            fechaActualizacion                  AS fechaActualizacion
        FROM planMantencionAnual
        WHERE idEquipo = @idEquipo
        AND habilitado = 1
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;


CREATE OR ALTER PROCEDURE spListarPlanMantencionPmaquinaDeshabilitados
    @idEquipo       INT
    AS

    BEGIN TRY
        SELECT
            idMantencion                        AS idMantencion,
            fechaCreacion                       AS fechaCreacion,
            descripcionMantencion               AS descripcionMantencion,
            areaMantencion                      AS areaMantencion,
            fechaActualizacion                  AS fechaActualizacion
        FROM planMantencionAnual
        WHERE idEquipo = @idEquipo
        AND habilitado = 0
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;

select * from planMantencionAnual;



