CREATE OR ALTER PROCEDURE spReporteMantencionMaquinas
    AS
    BEGIN TRY
        SELECT
            mm.idMantencionGeneral                          AS idMantencionMaquina,
            mm.fecha                                        AS fechaMantencion,
            mm.Descripcion                                  AS descripcionMantencion,
            mm.estado                                       AS estadoMantencion,
            mm.Tipo                                         AS tipoMantencion,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            maq.idMaquina                                   AS idMaquina,
            maq.nombreEquipo                                AS nombreMaquina,
            maq.marca                                       AS marcaMaquina,
            maq.AnioFabricacion                             AS anioFabricacion,
            maq.modelo                                      AS modeloMaquina,
            maq.peso                                        AS pesoMaquina,
            maq.rendimiento                                 AS rendimientoMaq,
            maq.fechaRecepcion                              AS fechaRecepcionMaquina,
            maq.medidas                                     AS medidasMaquina,
            maq.consumoAgua                                 AS consumoAguaMaquina,
            maq.Foto                                        AS foto,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionGeneralMaquina   mm
        JOIN turno                      t   ON t.idturno = mm.idTurno
        JOIN Usuario                    u   ON u.idUsuario = mm.idUsuario
        JOIN Maquina                    maq ON maq.idMaquina = mm.idMaquina
        JOIN SectorPlanta               s   ON s.idSector = maq.idSector
        JOIN Planta                     p   ON p.idPlanta = s.idPlanta
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spReporteMantencionMaquinasHoy
    @idturno        INT
    AS
    BEGIN TRY
        SELECT
            mm.idMantencionGeneral                          AS idMantencionMaquina,
            mm.fecha                                        AS fechaMantencion,
            mm.Descripcion                                  AS descripcionMantencion,
            mm.estado                                       AS estadoMantencion,
            mm.Tipo                                         AS tipoMantencion,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            maq.idMaquina                                   AS idMaquina,
            maq.nombreEquipo                                AS nombreMaquina,
            maq.marca                                       AS marcaMaquina,
            maq.AnioFabricacion                             AS anioFabricacion,
            maq.modelo                                      AS modeloMaquina,
            maq.peso                                        AS pesoMaquina,
            maq.rendimiento                                 AS rendimientoMaq,
            maq.fechaRecepcion                              AS fechaRecepcionMaquina,
            maq.medidas                                     AS medidasMaquina,
            maq.consumoAgua                                 AS consumoAguaMaquina,
            maq.Foto                                        AS foto,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionGeneralMaquina   mm
        JOIN turno                      t   ON t.idturno = mm.idTurno
        JOIN Usuario                    u   ON u.idUsuario = mm.idUsuario
        JOIN Maquina                    maq ON maq.idMaquina = mm.idMaquina
        JOIN SectorPlanta               s   ON s.idSector = maq.idSector
        JOIN Planta                     p   ON p.idPlanta = s.idPlanta
        WHERE CONVERT(date, mm.fecha) = CONVERT(date, GETDATE())
        AND t.idturno = @idturno
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spReporteMantencionMaquinasAyer
    @idturno        INT
    AS
    BEGIN TRY
        SELECT
            mm.idMantencionGeneral                          AS idMantencionMaquina,
            mm.fecha                                        AS fechaMantencion,
            mm.Descripcion                                  AS descripcionMantencion,
            mm.estado                                       AS estadoMantencion,
            mm.Tipo                                         AS tipoMantencion,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            maq.idMaquina                                   AS idMaquina,
            maq.nombreEquipo                                AS nombreMaquina,
            maq.marca                                       AS marcaMaquina,
            maq.AnioFabricacion                             AS anioFabricacion,
            maq.modelo                                      AS modeloMaquina,
            maq.peso                                        AS pesoMaquina,
            maq.rendimiento                                 AS rendimientoMaq,
            maq.fechaRecepcion                              AS fechaRecepcionMaquina,
            maq.medidas                                     AS medidasMaquina,
            maq.consumoAgua                                 AS consumoAguaMaquina,
            maq.Foto                                        AS foto,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionGeneralMaquina   mm
        JOIN turno                      t   ON t.idturno = mm.idTurno
        JOIN Usuario                    u   ON u.idUsuario = mm.idUsuario
        JOIN Maquina                    maq ON maq.idMaquina = mm.idMaquina
        JOIN SectorPlanta               s   ON s.idSector = maq.idSector
        JOIN Planta                     p   ON p.idPlanta = s.idPlanta
        WHERE CONVERT(date, mm.fecha) = CONVERT(date, DATEADD(day, -1, GETDATE()))
        AND t.idturno = @idturno
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spReporteMantencionMaquinasSemanal
    @idturno            INT
    AS
    BEGIN TRY
        SELECT
            mm.idMantencionGeneral                          AS idMantencionMaquina,
            mm.fecha                                        AS fechaMantencion,
            mm.Descripcion                                  AS descripcionMantencion,
            mm.estado                                       AS estadoMantencion,
            mm.Tipo                                         AS tipoMantencion,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            maq.idMaquina                                   AS idMaquina,
            maq.nombreEquipo                                AS nombreMaquina,
            maq.marca                                       AS marcaMaquina,
            maq.AnioFabricacion                             AS anioFabricacion,
            maq.modelo                                      AS modeloMaquina,
            maq.peso                                        AS pesoMaquina,
            maq.rendimiento                                 AS rendimientoMaq,
            maq.fechaRecepcion                              AS fechaRecepcionMaquina,
            maq.medidas                                     AS medidasMaquina,
            maq.consumoAgua                                 AS consumoAguaMaquina,
            maq.Foto                                        AS foto,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionGeneralMaquina   mm
        JOIN turno                      t   ON t.idturno = mm.idTurno
        JOIN Usuario                    u   ON u.idUsuario = mm.idUsuario
        JOIN Maquina                    maq ON maq.idMaquina = mm.idMaquina
        JOIN SectorPlanta               s   ON s.idSector = maq.idSector
        JOIN Planta                     p   ON p.idPlanta = s.idPlanta
        WHERE CONVERT(date, mm.fecha) >= CONVERT(date, DATEADD(day, -7, GETDATE()))
        AND CONVERT(date, mm.fecha) <= CONVERT(date, GETDATE())
        AND t.idturno = @idturno;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spReporteMantencionMaquinasMensual
    @idTurno                INT
    AS
    BEGIN TRY
        SELECT
            mm.idMantencionGeneral                          AS idMantencionMaquina,
            mm.fecha                                        AS fechaMantencion,
            mm.Descripcion                                  AS descripcionMantencion,
            mm.estado                                       AS estadoMantencion,
            mm.Tipo                                         AS tipoMantencion,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            maq.idMaquina                                   AS idMaquina,
            maq.nombreEquipo                                AS nombreMaquina,
            maq.marca                                       AS marcaMaquina,
            maq.AnioFabricacion                             AS anioFabricacion,
            maq.modelo                                      AS modeloMaquina,
            maq.peso                                        AS pesoMaquina,
            maq.rendimiento                                 AS rendimientoMaq,
            maq.fechaRecepcion                              AS fechaRecepcionMaquina,
            maq.medidas                                     AS medidasMaquina,
            maq.consumoAgua                                 AS consumoAguaMaquina,
            maq.Foto                                        AS foto,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionGeneralMaquina   mm
        JOIN turno                      t   ON t.idturno = mm.idTurno
        JOIN Usuario                    u   ON u.idUsuario = mm.idUsuario
        JOIN Maquina                    maq ON maq.idMaquina = mm.idMaquina
        JOIN SectorPlanta               s   ON s.idSector = maq.idSector
        JOIN Planta                     p   ON p.idPlanta = s.idPlanta
        WHERE YEAR(mm.fecha) = YEAR(GETDATE()) AND MONTH(mm.fecha) = MONTH(GETDATE())
        AND t.idturno = @idTurno
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;

