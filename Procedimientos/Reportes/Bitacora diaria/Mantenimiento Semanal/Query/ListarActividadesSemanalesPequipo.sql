CREATE OR ALTER PROCEDURE spListarAcitivadadesSemanalesPequipo
    @idEquipo           INT,
    @estado             INT
    AS

    BEGIN TRY
        SELECT
            b.idBitacora                          idActividad,
            b.descripcion                         descripcionActividad,
            b.habilitado                          estadoActividad,
            b.tipoActividad                       tipoActividad,
            m.idMaquina                           idEquipo,
            m.nombreEquipo                        nombreMaquina
        FROM BitacoraDiaria b
        JOIN Maquina M on b.idMaquina = M.idMaquina
        WHERE m.idMaquina = @idEquipo
        AND   b.habilitado = @estado
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;





















