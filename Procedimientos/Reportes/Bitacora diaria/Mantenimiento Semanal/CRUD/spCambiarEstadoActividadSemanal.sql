CREATE OR ALTER PROCEDURE spCambiarEstadoActividadSemanal
    @idActividad        INT,
    @estado             INT
    AS

    BEGIN TRY

        DECLARE @respuesta  INT

        UPDATE BitacoraDiaria
        SET habilitado = @estado
        WHERE idBitacora = @idActividad

        SET @respuesta = 1
        RETURN @respuesta
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
        SET @respuesta = 3
        RETURN @respuesta
    END CATCH
GO;
