CREATE OR ALTER PROCEDURE spConsultaNovedadesDiarias
    @area           VARCHAR(50)
    AS
    BEGIN TRY

        SELECT
            idMantencionGeneral         AS idMantencion,
            Descripcion                 AS descripcion,
            'maquina'                   AS tipo,
            'Novedad'                   AS clasificacion,
            m.nombreEquipo              AS realizadaA,
            t.nombreTurno               AS turno,
            mm.fecha                    AS fecha,
            us.nombreusuario            AS nombreusuario,
            mm.horaInicio               AS horaInicio,
            mm.HoraTermino              AS horaTermino,
            mm.minutosUsados            AS DuracionMinutos
        FROM MantencionGeneralMaquina   mm
        JOIN Maquina                    m   ON mm.idMaquina = m.idMaquina
        JOIN turno                      t   ON mm.idTurno = t.idturno
        JOIN Usuario                    Us  ON us.idUsuario = mm.idUsuario
        WHERE mm.Tipo LIKE 'Novedad'
        AND   CAST(mm.fecha AS DATE) = CAST(GETDATE() AS DATE)
        AND mm.area LIKE @area

        UNION ALL
        SELECT
            idMantencionSector          AS idMantencion,
            Descripcion                 AS descripcion,
            'Sector'                    AS tipo,
            'Novedad'                   AS clasificacion,
            s.nombreSector              AS realizadaA,
            t2.nombreTurno              AS turno,
            ms.fecha                    AS fecha,
            us.nombreusuario            AS nombreusuario,
            ms.horaInicio               AS horaInicio,
            ms.HoraTermino              AS horaTermino,
            ms.minutosUsados            AS DuracionMinutos
        FROM MantencionSector           ms
        JOIN SectorPlanta               s   ON ms.idSector = s.idSector
        JOIN turno                      t2  ON ms.idTurno = t2.idturno
        JOIN Usuario                    Us  ON us.idUsuario = ms.idUsuario
        WHERE ms.Tipo LIKE 'Novedad'
        AND   CAST(ms.fecha AS DATE) = CAST(GETDATE() AS DATE)

        UNION ALL
        SELECT
            idObservacionMaquina        AS idMantencion,
            Descripcion                 AS descripcion,
            'maquina'                   AS tipo,
            'Observacion'               AS clasificacion,
            m.nombreEquipo              AS realizadaA,
            t.nombreTurno               AS turno,
            mm.fecha                    AS fecha,
            us.nombreusuario            AS nombreusuario,
            mm.horaInicio               AS horaInicio,
            mm.HoraTermino              AS horaTermino,
            mm.minutosUsados            AS DuracionMinutos
        FROM ObservacionMaquina         mm
        JOIN Maquina                    m   ON mm.idMaquina = m.idMaquina
        JOIN turno                      t   ON mm.idTurno = t.idturno
        JOIN Usuario                    Us  ON us.idUsuario = mm.idUsuario
        WHERE CAST(mm.fecha AS DATE) = CAST(GETDATE() AS DATE)
        AND mm.area LIKE @area

        UNION ALL
        SELECT
            idObservacionSector         AS idMantencion,
            Descripcion                 AS descripcion,
            'Sector'                    AS tipo,
            'Observacion'               AS clasificacion,
            s.nombreSector              AS realizadaA,
            t2.nombreTurno              AS turno,
            ms.fecha                    AS fecha,
            us.nombreusuario            AS nombreusuario,
            ms.horaInicio               AS horaInicio,
            ms.HoraTermino              AS horaTermino,
            ms.minutosUsados            AS DuracionMinutos
        FROM ObservacionSector          ms
        JOIN SectorPlanta               s   ON ms.idSector = s.idSector
        JOIN turno                      t2  ON ms.idTurno = t2.idturno
        JOIN Usuario                    Us  ON us.idUsuario = ms.idUsuario
        WHERE CAST(ms.fecha AS DATE) = CAST(GETDATE() AS DATE)

    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion
    END CATCH
GO;







