CREATE OR ALTER PROCEDURE spReporteMantencionSectores
    AS
    BEGIN TRY
        SELECT
            ms.idMantencionSector                           AS idMantencion,
            ms.fecha                                        AS fechaMantencion,
            ms.Descripcion                                  AS descripcionMantencion,
            ms.estado                                       AS estadoMantencion,
            ms.Tipo                                         AS tipoMantencion,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionSector           ms
        JOIN SectorPlanta               s  ON s.idSector = ms.idSector
        JOIN Usuario                    u  ON u.idUsuario = ms.idUsuario
        JOIN Turno                      t  ON t.idturno = ms.idTurno
        JOIN Planta                     P  ON s.idPlanta = P.idPlanta
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spReporteMantencionSectoresHoy
    @idTurno            INT
    AS
    BEGIN TRY
        SELECT
            ms.idMantencionSector                           AS idMantencion,
            ms.fecha                                        AS fechaMantencion,
            ms.Descripcion                                  AS descripcionMantencion,
            ms.estado                                       AS estadoMantencion,
            ms.Tipo                                         AS tipoMantencion,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionSector           ms
        JOIN SectorPlanta               s  ON s.idSector = ms.idSector
        JOIN Usuario                    u  ON u.idUsuario = ms.idUsuario
        JOIN Turno                      t  ON t.idturno = ms.idTurno
        JOIN Planta                     P  ON s.idPlanta = P.idPlanta
        WHERE CONVERT(date, ms.fecha) = CONVERT(date, GETDATE())
        AND t.idturno = @idturno
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spReporteMantencionSectoresAyer
    @idTurno            INT
    AS
    BEGIN TRY
        SELECT
            ms.idMantencionSector                           AS idMantencion,
            ms.fecha                                        AS fechaMantencion,
            ms.Descripcion                                  AS descripcionMantencion,
            ms.estado                                       AS estadoMantencion,
            ms.Tipo                                         AS tipoMantencion,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionSector           ms
        JOIN SectorPlanta               s  ON s.idSector = ms.idSector
        JOIN Usuario                    u  ON u.idUsuario = ms.idUsuario
        JOIN Turno                      t  ON t.idturno = ms.idTurno
        JOIN Planta                     P  ON s.idPlanta = P.idPlanta
        WHERE CONVERT(date, ms.fecha) = CONVERT(date, DATEADD(day, -1, GETDATE()))
        AND t.idturno = @idturno
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spReporteMantencionSectoresSemanal
    @idTurno            INT
    AS
    BEGIN TRY
        SELECT
            ms.idMantencionSector                           AS idMantencion,
            ms.fecha                                        AS fechaMantencion,
            ms.Descripcion                                  AS descripcionMantencion,
            ms.estado                                       AS estadoMantencion,
            ms.Tipo                                         AS tipoMantencion,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionSector           ms
        JOIN SectorPlanta               s  ON s.idSector = ms.idSector
        JOIN Usuario                    u  ON u.idUsuario = ms.idUsuario
        JOIN Turno                      t  ON t.idturno = ms.idTurno
        JOIN Planta                     P  ON s.idPlanta = P.idPlanta
        WHERE CONVERT(date, ms.fecha) >= CONVERT(date, DATEADD(day, -7, GETDATE()))
        AND CONVERT(date, ms.fecha) <= CONVERT(date, GETDATE())
        AND t.idturno = @idturno;
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;

CREATE OR ALTER PROCEDURE spReporteMantencionSectoresMensual
    @idTurno            INT
    AS
    BEGIN TRY
        SELECT
            ms.idMantencionSector                           AS idMantencion,
            ms.fecha                                        AS fechaMantencion,
            ms.Descripcion                                  AS descripcionMantencion,
            ms.estado                                       AS estadoMantencion,
            ms.Tipo                                         AS tipoMantencion,
            s.idSector                                      AS idSector,
            s.nombreSector                                  AS nombreSector,
            s.Encargado                                     AS encargadoSector,
            s.Habilitado                                    AS habilitadoSector,
            u.idUsuario                                     AS idUsuario,
            u.nombreusuario                                 AS nombreUsuario,
            t.idturno                                       AS idTurno,
            t.nombreTurno                                   AS nombreTurno,
            t.nombreAlt                                     AS nombreTurnoAlt,
            p.idPlanta                                      AS idPlanta,
            p.nombrePlanta                                  AS nombrePlanta,
            p.direccionPlanta                               AS direccionPlanta,
            p.telefonoPlanta                                AS telefonoPlanta,
            p.habilitad                                     AS habilitadoPlanta
        FROM MantencionSector           ms
        JOIN SectorPlanta               s  ON s.idSector = ms.idSector
        JOIN Usuario                    u  ON u.idUsuario = ms.idUsuario
        JOIN Turno                      t  ON t.idturno = ms.idTurno
        JOIN Planta                     P  ON s.idPlanta = P.idPlanta
        WHERE YEAR(ms.fecha) = YEAR(GETDATE()) AND MONTH(ms.fecha) = MONTH(GETDATE())
        AND t.idturno = @idTurno
    END TRY
    BEGIN CATCH
        DECLARE @procedimiento VARCHAR(200)
        DECLARE @numeroPro     VARCHAR(30)
        DECLARE @descripcion   NVARCHAR(MAX)
        DECLARE @fechapro      DATETIME;

        SET @procedimiento = ERROR_PROCEDURE();
        SET @numeroPro = ERROR_NUMBER();
        SET @descripcion = ERROR_MESSAGE();
        SET @fechapro = GETDATE();
        EXEC spInsertarError @procedimiento,@numeroPro,@descripcion,@fechapro
    END CATCH
GO;









