CREATE OR ALTER PROCEDURE spInsertarError

    @nombreProcedimiento     VARCHAR(200),
    @numeroProcedimiento     VARCHAR(30),
    @descripcionError        NVARCHAR(MAX)

    AS
        BEGIN TRY
            INSERT INTO errores
            VALUES(@nombreProcedimiento,@numeroProcedimiento,@descripcionError, GETDATE())
        END TRY

        BEGIN CATCH
            INSERT INTO errores
            values(ERROR_PROCEDURE(), ERROR_NUMBER(), ERROR_MESSAGE(), GETDATE())
        end catch
GO;



