DECLARE @counter INT = 1;
DECLARE @MaquinaID INT;

-- Asigna el valor deseado a @MaquinaID
SET @MaquinaID = 22; -- Puedes cambiar este valor según tus necesidades.

WHILE @counter <= 20  -- Puedes cambiar este número para especificar cuántas filas deseas insertar.
BEGIN
    DECLARE @RandomDescription NVARCHAR(100);
    SET @RandomDescription =
        CASE ABS(CHECKSUM(NEWID()) % 10)
            WHEN 0 THEN 'Mantenimiento preventivo de equipo ' + CAST(@counter AS NVARCHAR(10))
            WHEN 1 THEN 'Revisión de equipo ' + CAST(@counter AS NVARCHAR(10))
            WHEN 2 THEN 'Inspección rutinaria de equipo ' + CAST(@counter AS NVARCHAR(10))
            WHEN 3 THEN 'Mantenimiento general de equipo ' + CAST(@counter AS NVARCHAR(10))
            WHEN 4 THEN 'Reparación de equipo ' + CAST(@counter AS NVARCHAR(10))
            WHEN 5 THEN 'Calibración de equipo ' + CAST(@counter AS NVARCHAR(10))
            WHEN 6 THEN 'Sustitución de piezas en equipo ' + CAST(@counter AS NVARCHAR(10))
            WHEN 7 THEN 'Lubricación de equipo ' + CAST(@counter AS NVARCHAR(10))
            WHEN 8 THEN 'Ajuste de maquinaria ' + CAST(@counter AS NVARCHAR(10))
            ELSE 'Control de calidad de equipo ' + CAST(@counter AS NVARCHAR(10))
        END;

    DECLARE @RandomArea NVARCHAR(20);
    SET @RandomArea =
        CASE ABS(CHECKSUM(NEWID()) % 3)
            WHEN 0 THEN 'Mecanico'
            WHEN 1 THEN 'Frigorista'
            ELSE 'Electricista'
        END;

    DECLARE @RandomDate DATETIME;
    SET @RandomDate = DATEADD(DAY, -1 * ABS(CHECKSUM(NEWID()) % 365), GETDATE());

    INSERT INTO [dbo].[MantencionGeneralMaquina] ([idMaquina], [idUsuario], [idTurno], [fecha], [Descripcion], [estado], [Tipo], [area], [horaInicio], [HoraTermino], [comentarioAdicional])
    VALUES (@MaquinaID, 1003, 1, @RandomDate,
            @RandomDescription,
            'Completada', 'Novedad', @RandomArea,
            CAST('12:59:01.2700000' AS Time),
            CAST('13:59:01.2700000' AS Time),
            'Sin Comentarios Adicionales');

    SET @counter = @counter + 1;
END;
