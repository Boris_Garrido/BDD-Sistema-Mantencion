drop table BitacoraDiaria;
drop table DetalleMantencionEquipo;
drop table DetalleMantencionSector;
drop table DetalleObservacionMaquina;
drop table DetalleObservacionSector;
drop table MantencionExterna;
drop table ManualMaquinas;
drop table ObservacionMaquina;
drop table ObservacionSector;
drop table MantencionSector;
drop table MantencionGeneralMaquina;
drop table ActividadesGenericasSector;
drop table ActividadesGenericasMaquina;
drop table ActividadesGenericasSistema;
drop table OrdenTrabajo;
drop table PlanMantencionAnual;
drop table OtMantencionSector;
drop table OtMantencionMaquina;
drop table OtMantencionSubSistema;
drop table Subsistema;
drop table Maquina;
drop table SectorPlanta
drop table Planta


INSERT INTO PLANTA VALUES(
                          'Planta Merluza',
                          'Gran Bretaña',
                          '56414243',
                          1
                         )


INSERT INTO SectorPlanta VALUES (
                         1,
                         'Sector Corte',
                         'Juan',
                         1
)

INSERT INTO SectorPlanta VALUES (
                         1,
                         'Sector 2',
                         'Juan',
                         1
)


INSERT INTO SectorPlanta VALUES (
                         1,
                         'Sector 3',
                         'Juan',
                         1
)



INSERT INTO Maquina VALUES (

                            1,
                            'Fileteadora',
                            'Baader',
                            '2003',
                            '208',
                            '1225 kg',
                            '38 a 45 pescados por minuto',
                            '2003',
                            'L:3760 mm  A:940mm  Al:1540 mm',
                            '25 L / por min',
                            null
                           )



INSERT INTO Subsistema VALUES (
                               1,
                               'Sistema Electrico',
                               '380 VOlt',
                               '2,6 Kw',
                               '3',
                               '10 Amp',
                               '50 Hz',
                               '25 L / Por Min'
                              )

INSERT INTO Subsistema VALUES (
                               1,
                               'Sistema Mecanico',
                               '380 VOlt',
                               '2,6 Kw',
                               '3',
                               '10 Amp',
                               '50 Hz',
                               '25 L / Por Min'
                              )

INSERT INTO Subsistema VALUES (
                               1,
                               'Sistema Informatico',
                               '380 VOlt',
                               '2,6 Kw',
                               '3',
                               '10 Amp',
                               '50 Hz',
                               '25 L / Por Min'
                              )

INSERT INTO Subsistema VALUES (
                               1,
                               'Sistema general',
                               '380 VOlt',
                               '2,6 Kw',
                               '3',
                               '10 Amp',
                               '50 Hz',
                               '25 L / Por Min'
                              )

INSERT INTO Subsistema VALUES (
                               1,
                               'Sistema Test',
                               '380 VOlt',
                               '2,6 Kw',
                               '3',
                               '10 Amp',
                               '50 Hz',
                               '25 L / Por Min'
                              )

INSERT INTO Mecanico VALUES ('Juan',
                             '12388992-1',
                             1
                             )

INSERT INTO OrdenTrabajo VALUES (
                                 1,
                                 1,
                                 GETDATE(),
                                 'Afilado de cuchillos',
                                 'Realizada'
                                )



INSERT INTO PlanMantencionAnual values (
                                        1,
                                        'Revisar estado de motores electricos',
                                        GETDATE(),
                                        1
                                       )

INSERT INTO PlanMantencionAnual values (
                                        1,
                                        'Revisar cajas reductoras, cambio de rodamientos, retenes y cambio de aceite',
                                        GETDATE(),
                                        0
                                       )

INSERT INTO PlanMantencionAnual values (
                                        1,
                                        'Revisar estado de resortes, levas y silletas',
                                        GETDATE(),
                                        1
                                       )

INSERT INTO PlanMantencionAnual values (
                                        1,
                                        'Revisar riel de guia en extendor de filetes',
                                        GETDATE(),
                                        0
                                       )

INSERT INTO PlanMantencionAnual values (
                                        1,
                                        'Cambiar piezas de desgaste, guia de aletas y soporte de seccion',
                                        GETDATE(),
                                        1
                                       )

INSERT INTO PlanMantencionAnual values (
                                        1,
                                        'Revisar estado o cambiar rodillos de levas en accionamientos de cuchillos de ijada',
                                        GETDATE(),
                                        0
                                       )







SELECT

    b.idBitacora                            AS idActividad,
    b.descripcion                           AS descripcion,
    m.idMaquina                             AS idMaquina,
    m.nombreEquipo                          AS nombreEquipo




FROM BitacoraDiaria             b
JOIN Maquina                    m   ON m.idMaquina = b.idMaquina


select * from Maquina;

select * from Planta

update Planta


