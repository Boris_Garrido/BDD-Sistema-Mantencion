CREATE TABLE BitacoraDiaria(

    idBitacora              INT IDENTITY(1,1) PRIMARY KEY,
    idMaquina               INT FOREIGN KEY REFERENCES Maquina(idMaquina),
    descripcion             VARCHAR(600) NOT NULL,
    tipoActividad           VARCHAR(30),
    habilitado              BIT DEFAULT 1
)


CREATE INDEX ixBitacora ON BitacoraDiaria(idBitacora)

select * from Maquina

INSERT INTO BitacoraDiaria values (6, 'Control de Funcionamiento', 'Diaria');
INSERT INTO BitacoraDiaria values (7, 'Control de Funcionamiento', 'Diaria');
INSERT INTO BitacoraDiaria values (8, 'Control de Funcionamiento', 'Diaria');


INSERT INTO BitacoraDiaria values (4, 'Revision de Estados de resortes y tension de compresion', 'Diaria');
INSERT INTO BitacoraDiaria values (4, 'Lubricacion de Piezas de Alta Velocidad', 'Diaria');
INSERT INTO BitacoraDiaria values (4, 'Control de Funcionamiento de Equipo / Sistema de Vacio', 'Diaria');

INSERT INTO BitacoraDiaria values (5, 'Revision de Estados de resortes y tension de compresion', 'Diaria');
INSERT INTO BitacoraDiaria values (5, 'Lubricacion de Piezas de Alta Velocidad', 'Diaria');
INSERT INTO BitacoraDiaria values (5, 'Control de Funcionamiento de Equipo / Sistema de Vacio', 'Diaria');

INSERT INTO BitacoraDiaria values (6, 'Cambio de Cuchillos', 'Diaria');
INSERT INTO BitacoraDiaria values (6, 'Lubricacion de Piezas de Alta Velocidad', 'Diaria');
INSERT INTO BitacoraDiaria values (6, 'Control de Funcionamiento y Regulaciones Menores', 'Diaria');
INSERT INTO BitacoraDiaria values (6, 'Afilado de Cuchillos', 'Diaria');

INSERT INTO BitacoraDiaria values (7, 'Cambio de Cuchillos', 'Diaria');
INSERT INTO BitacoraDiaria values (7, 'Lubricacion de Piezas de Alta Velocidad', 'Diaria');
INSERT INTO BitacoraDiaria values (7, 'Control de Funcionamiento y Regulaciones Menores', 'Diaria');
INSERT INTO BitacoraDiaria values (7, 'Afilado de Cuchillos', 'Diaria');

INSERT INTO BitacoraDiaria values (8, 'Cambio de Cuchillos', 'Diaria');
INSERT INTO BitacoraDiaria values (8, 'Lubricacion de Piezas de Alta Velocidad', 'Diaria');
INSERT INTO BitacoraDiaria values (8, 'Control de Funcionamiento y Regulaciones Menores', 'Diaria');
INSERT INTO BitacoraDiaria values (8, 'Afilado de Cuchillos', 'Diaria');

INSERT INTO BitacoraDiaria values (9, 'Cambio de Cuchillos', 'Diaria');
INSERT INTO BitacoraDiaria values (9, 'Lubricacion de Piezas de Alta Velocidad', 'Diaria');
INSERT INTO BitacoraDiaria values (9, 'Control de Funcionamiento y Regulaciones Menores', 'Diaria');
INSERT INTO BitacoraDiaria values (9, 'Afilado de Cuchillos', 'Diaria');


INSERT INTO BitacoraDiaria values (10, 'Limpieza y Afilado de Cuchillos', 'Diaria');
INSERT INTO BitacoraDiaria values (10, 'Inspeccion Visual de Estructura', 'Diaria');
INSERT INTO BitacoraDiaria values (10, 'Retiro de Guia de cuchillo y limpieza', 'Diaria');

INSERT INTO BitacoraDiaria values (11, 'Limpieza y Afilado de Cuchillos', 'Diaria');
INSERT INTO BitacoraDiaria values (11, 'Inspeccion Visual de Estructura', 'Diaria');
INSERT INTO BitacoraDiaria values (11, 'Retiro de Guia de cuchillo y limpieza', 'Diaria');

INSERT INTO BitacoraDiaria values (12, 'Limpieza y Afilado de Cuchillos', 'Diaria');
INSERT INTO BitacoraDiaria values (12, 'Inspeccion Visual de Estructura', 'Diaria');
INSERT INTO BitacoraDiaria values (12, 'Retiro de Guia de cuchillo y limpieza', 'Diaria');

INSERT INTO BitacoraDiaria values (13, 'Revisar Estado de rasero, cinta machacadora y tambor perforador', 'Diaria');
INSERT INTO BitacoraDiaria values (13, 'Control de Funcionamiento', 'Diaria');

INSERT INTO BitacoraDiaria values (14, 'Control de Funcionamiento', 'Diaria');
INSERT INTO BitacoraDiaria values (15, 'Control de Funcionamiento', 'Diaria');

INSERT INTO BitacoraDiaria values (16, 'Ejercicios Neumaticos', 'Diaria');
INSERT INTO BitacoraDiaria values (16, 'Control de Presion de Aire', 'Diaria');

INSERT INTO BitacoraDiaria values (17, 'Ejercicios Neumaticos', 'Diaria');
INSERT INTO BitacoraDiaria values (17, 'Control de Presion de Aire', 'Diaria');

INSERT INTO BitacoraDiaria values (18, 'Ejercicios Neumaticos', 'Diaria');
INSERT INTO BitacoraDiaria values (18, 'Control de Presion de Aire', 'Diaria');

INSERT INTO BitacoraDiaria values (19, 'Control de Funcionamiento', 'Diaria');
INSERT INTO BitacoraDiaria values (20, 'Control de Funcionamiento', 'Diaria');
INSERT INTO BitacoraDiaria values (21, 'Control de Funcionamiento', 'Diaria');
INSERT INTO BitacoraDiaria values (22, 'Control de Funcionamiento', 'Diaria');

INSERT INTO BitacoraDiaria values (23, 'Inspeccion Visual de Estructura', 'Diaria');
INSERT INTO BitacoraDiaria values (23, 'Control de Temperatura', 'Diaria');
INSERT INTO BitacoraDiaria values (23, 'Control de Funcionamiento', 'Diaria');

INSERT INTO BitacoraDiaria values (24, 'Inspeccion Visual de Estructura', 'Diaria');
INSERT INTO BitacoraDiaria values (24, 'Control de Temperatura', 'Diaria');
INSERT INTO BitacoraDiaria values (24, 'Control de Funcionamiento', 'Diaria');

INSERT INTO BitacoraDiaria values (25, 'Inspeccion Visual de Estructura', 'Diaria');
INSERT INTO BitacoraDiaria values (25, 'Control de Temperatura', 'Diaria');
INSERT INTO BitacoraDiaria values (25, 'Control de Funcionamiento', 'Diaria');

INSERT INTO BitacoraDiaria values (26, 'Inspeccion Visual de Estructura', 'Diaria');
INSERT INTO BitacoraDiaria values (26, 'Control de Temperatura', 'Diaria');
INSERT INTO BitacoraDiaria values (26, 'Control de Funcionamiento', 'Diaria');

INSERT INTO BitacoraDiaria values (27, 'Inspeccion Visual de Estructura', 'Diaria');
INSERT INTO BitacoraDiaria values (27, 'Control de Temperatura', 'Diaria');
INSERT INTO BitacoraDiaria values (27, 'Control de Funcionamiento', 'Diaria');

INSERT INTO BitacoraDiaria values (28, 'Inspeccion Visual de Estructura', 'Diaria');
INSERT INTO BitacoraDiaria values (28, 'Control de Temperatura', 'Diaria');
INSERT INTO BitacoraDiaria values (28, 'Control de Funcionamiento', 'Diaria');

INSERT INTO BitacoraDiaria values (29, 'Revision nivel de tinta', 'Diaria');
INSERT INTO BitacoraDiaria values (29, 'Revision nivel de aditivo', 'Diaria');

INSERT INTO BitacoraDiaria values (30, 'Revision nivel de tinta', 'Diaria');
INSERT INTO BitacoraDiaria values (30, 'Revision nivel de aditivo', 'Diaria');

INSERT INTO BitacoraDiaria values (31, 'Revision nivel de tinta', 'Diaria');
INSERT INTO BitacoraDiaria values (31, 'Revision nivel de aditivo', 'Diaria');

INSERT INTO BitacoraDiaria values (32, 'Revision nivel de tinta', 'Diaria');
INSERT INTO BitacoraDiaria values (32, 'Revision nivel de aditivo', 'Diaria');

INSERT INTO BitacoraDiaria values (33, 'Revision de funcionamiento y Ciclos', 'Diaria');
INSERT INTO BitacoraDiaria values (34, 'Revision de funcionamiento y Ciclos', 'Diaria');
INSERT INTO BitacoraDiaria values (35, 'Revision de funcionamiento y Ciclos', 'Diaria');



select * from MantencionSector;

select * from MantencionGeneralMaquina;


insert into MantencionSector values (2,1,1,getdate(),'test 6','completada','Novedad')
insert into MantencionGeneralMaquina values (10,1,1,getdate(), 'Test 5', 'Completada', 'Novedad')

SELECT
        idMantencionGeneral         AS idMantencion,
        Descripcion                 AS descripcion,
        'maquina'                   AS tipo,
        m.nombreEquipo              AS realizadaA,
        mm.fecha                    AS fecha
FROM MantencionGeneralMaquina   mm
JOIN Maquina                    m   ON mm.idMaquina = m.idMaquina
WHERE mm.Tipo LIKE 'Novedad'
AND   CAST(mm.fecha AS DATE) = CAST(GETDATE() AS DATE)

UNION ALL

SELECT
        idMantencionSector      AS idMantencion,
        Descripcion             AS descripcion,
        'Sector'                AS tipo,
        s.nombreSector          AS realizadaA,
        ms.fecha                AS fecha
FROM MantencionSector           ms
join SectorPlanta               s   ON ms.idSector = s.idSector
where ms.Tipo LIKE 'Novedad'
AND   CAST(ms.fecha AS DATE) = CAST(GETDATE() AS DATE);
