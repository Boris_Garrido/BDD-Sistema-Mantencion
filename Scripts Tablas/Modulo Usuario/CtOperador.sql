CREATE TABLE Operador(
    idOperador          INT PRIMARY KEY IDENTITY (1,1),
    nombreOperador      NVARCHAR(250),
    tipo                NVARCHAR(250),
    habilitado          BIT
)

CREATE INDEX IxOperador ON Operador(idOperador);


SELECT * FROM Operador;

drop table Operador;