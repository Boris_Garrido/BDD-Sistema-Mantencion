CREATE TABLE Usuario
(
    idUsuario               INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    nombreusuario           NVARCHAR(200) NOT NUll,
    contrasenia             BINARY(64) NOT NULL,
    tipoUsuario             VARCHAR(50),
    identificador           UNIQUEIDENTIFIER,
    habilitado              BIT
)


