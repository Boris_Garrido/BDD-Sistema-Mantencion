CREATE SEQUENCE seqManual
START WITH 1
INCREMENT BY 1;

CREATE TABLE ManualMaquinas (
    idManual            INT PRIMARY KEY DEFAULT(Next Value For seqManual),
    idMaquina           INT FOREIGN KEY references Maquina(idMaquina),
    fechaCreacion       DATETIME DEFAULT GETDATE(),
    descripcionManual   NVARCHAR(1000),
    rutaArchivo         NVARCHAR(1000),
    habilitado          BIT
)


create Index IxManuales ON ManualMaquinas(idManual)

