

CREATE TABLE ObservacionMaquina(

    idObservacionMaquina            INT  PRIMARY KEY  IDENTITY (1,1),
    idMaquina                       INT FOREIGN KEY REFERENCES Maquina(idMaquina),
    idUsuario                       INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    idTurno                         INT FOREIGN KEY REFERENCES turno(idturno),
    fecha                           DATETIME,
    Descripcion                     VARCHAR(1000),
    estado                          VARCHAR(200),
    Tipo                            VARCHAR(30) NOT NULL,
    area                            VARCHAR(50),
    Notificada                      BIT,
    horaInicio                      TIME,
    HoraTermino                     TIME,
    minutosUsados                   AS        ( CASE
                                                WHEN HoraInicio IS NOT NULL AND HoraTermino IS NOT NULL THEN DATEDIFF(MINUTE, HoraInicio, HoraTermino)
                                                ELSE 0
                                                END),
    FinalizadaPor                   INT FOREIGN KEY REFERENCES Usuario(idUsuario) NULL,
    ultimaActualizacion             DATETIME DEFAULT GETDATE(),
    idOrdenTrabajo                  INT FOREIGN KEY references OtMantencionSector,
    comentarioEstado                NVARCHAR(1000)
)

CREATE INDEX IxObservacionMaquina ON ObservacionMaquina(idObservacionMaquina);





drop table ObservacionMaquina;