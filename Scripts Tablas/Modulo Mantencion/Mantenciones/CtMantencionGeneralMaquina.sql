CREATE TABLE MantencionGeneralMaquina(

    idMantencionGeneral             INT IDENTITY(1,1) PRIMARY KEY,
    idMaquina                       INT FOREIGN KEY REFERENCES Maquina(idMaquina),
    idUsuario                       INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    idTurno                         INT FOREIGN KEY REFERENCES turno(idturno),
    fecha                           DATETIME,
    Descripcion                     VARCHAR(1000),
    estado                          VARCHAR(200),
    Tipo                            VARCHAR(30) NOT NULL,
    area                            VARCHAR(50),
    horaInicio                      TIME,
    HoraTermino                     TIME,
    minutosUsados                   AS DATEDIFF(minute,[horaInicio], [horaTermino]),
    comentarioAdicional             NVARCHAR(1000) DEFAULT 'Sin Comentarios Adicionales'
)


CREATE INDEX IxIdMantencionGeneral ON MantencionGeneralMaquina(idMantencionGeneral);


drop table MantencionGeneralMaquina;
