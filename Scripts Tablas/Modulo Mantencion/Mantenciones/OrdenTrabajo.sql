CREATE TABLE OrdenTrabajo(

    idOrdenTrabajo              INT IDENTITY(1,1) PRIMARY KEY,
    idSubSistema                INT FOREIGN KEY REFERENCES Subsistema(idSubSistema),
    idusuario                   INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    idTurno                     INT FOREIGN KEY REFERENCES turno(idturno),
    fecha                       DATETIME,
    Descripcion                 VARCHAR(1000),
    estado                      VARCHAR(200),
    tipoOt                      VARCHAR(200),
    area                        VARCHAR(50)
)


CREATE INDEX IxIdOt ON OrdenTrabajo(idOrdenTrabajo);


drop table OrdenTrabajo;
