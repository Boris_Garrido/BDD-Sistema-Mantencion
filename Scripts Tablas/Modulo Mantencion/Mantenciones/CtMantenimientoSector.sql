CREATE TABLE MantencionSector(

    idMantencionSector              INT IDENTITY(1,1) PRIMARY KEY,
    idSector                        INT FOREIGN KEY REFERENCES SectorPlanta(idSector),
    idUsuario                       INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    idTurno                         INT FOREIGN KEY REFERENCES turno(idturno),
    fecha                           DATETIME,
    Descripcion                     VARCHAR(1000),
    estado                          VARCHAR(200),
    Tipo                            VARCHAR(30) NOT NULL,
    area                            VARCHAR(50),
    horaInicio                      TIME,
    horaTermino                     TIME,
    minutosUsados                   AS DATEDIFF(minute,[horaInicio], [horaTermino])
)

CREATE INDEX IxIdMantencion ON MantencionSector(idMantencionSector);