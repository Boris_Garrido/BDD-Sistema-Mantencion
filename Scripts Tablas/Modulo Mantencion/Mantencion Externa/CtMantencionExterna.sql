CREATE TABLE MantencionExterna(

    idMantencionExterna         INT PRIMARY KEY IDENTITY (1,1),
    idEquipo                    INT FOREIGN KEY REFERENCES Maquina(idMaquina),
    empresaEncargada            VARCHAR(200),
    fechaIngreso                DATETIME DEFAULT GETDATE(),
    descripcionBreve            NVARCHAR(250) DEFAULT N'Sin descripción',
    rutaArchivoPdf              NVARCHAR(1000) NOT NULL,
    habilitado                  BIT
)

CREATE INDEX IxMantencionExterna ON MantencionExterna(idMantencionExterna);