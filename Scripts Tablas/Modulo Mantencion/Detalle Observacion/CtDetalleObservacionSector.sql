CREATE  TABLE DetalleObservacionSector(

    idDetalleObservacionSector     INT PRIMARY KEY IDENTITY (1,1),
    idOperador                     INT FOREIGN KEY REFERENCES Operador(idOperador),
    idObservacionSector            INT FOREIGN KEY REFERENCES ObservacionSector(idObservacionSector)
)


CREATE INDEX IxDetalleObservacionSector ON DetalleObservacionSector(idDetalleObservacionSector)