CREATE  TABLE DetalleObservacionMaquina(

    idDetalleObservacionMaquina     INT PRIMARY KEY IDENTITY (1,1),
    idOperador                      INT FOREIGN KEY REFERENCES Operador(idOperador),
    idObservacion                   INT FOREIGN KEY REFERENCES ObservacionMaquina(idObservacionMaquina)
)


CREATE INDEX IxDetalleObservacionMaquina ON DetalleObservacionMaquina(idDetalleObservacionMaquina)


select * from ObservacionMaquina