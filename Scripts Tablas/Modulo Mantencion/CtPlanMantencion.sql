CREATE TABLE PlanMantencionAnual(

    idMantencion                INT IDENTITY(1,1) PRIMARY KEY,
    idSubsistema                INT FOREIGN KEY REFERENCES Subsistema(idSubSistema),
    descripcionMantencion       VARCHAR(1000),
    Fecha                       DATETIME,
    Realizada                   BIT,
    habilitado                  BIT
)

CREATE INDEX IXidMantencionAnual ON PlanMantencionAnual(idMantencion)