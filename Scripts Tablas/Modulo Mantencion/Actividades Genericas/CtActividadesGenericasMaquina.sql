CREATE TABLE ActividadesGenericasMaquina(

    idActividadMaquina          INT PRIMARY KEY  IDENTITY (1,1),
    idMaquina                   INT FOREIGN KEY REFERENCES Maquina(idMaquina),
    descripcion                 VARCHAR(600)
)


CREATE INDEX IxGenericasMaquina ON ActividadesGenericasMaquina(idActividadMaquina)

INSERT INTO ActividadesGenericasMaquina values (1, 'Afilado de cuchillos');
INSERT INTO ActividadesGenericasMaquina values (1, 'Cambio de aceite');