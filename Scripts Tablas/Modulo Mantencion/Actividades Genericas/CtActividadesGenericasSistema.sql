CREATE TABLE ActividadesGenericasSistema(

    idActividadSistema          INT PRIMARY KEY  IDENTITY (1,1),
    idSubSistema                INT FOREIGN KEY REFERENCES Subsistema(idSubSistema),
    descripcion                 VARCHAR(600)
)


CREATE INDEX IxGenericasSistema ON ActividadesGenericasSistema(idActividadSistema)

INSERT INTO ActividadesGenericasSistema values (1, 'Revision elementos');
INSERT INTO ActividadesGenericasSistema values (1, 'Limpieza');