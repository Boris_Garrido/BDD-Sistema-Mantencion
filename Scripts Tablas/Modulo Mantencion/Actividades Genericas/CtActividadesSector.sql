CREATE TABLE ActividadesGenericasSector(

    idActividadSector           INT PRIMARY KEY  IDENTITY (1,1),
    idSector                    INT FOREIGN KEY REFERENCES SectorPlanta(idSector),
    descripcion                 VARCHAR(600)
)


CREATE INDEX ixGenericaSector ON ActividadesGenericasSector(idActividadSector)

insert into ActividadesGenericasSector values (1, 'Mantenimiento');
insert into ActividadesGenericasSector values (1, 'Cambio de pieza generica');