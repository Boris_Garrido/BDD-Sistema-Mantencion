CREATE TABLE OtMantencionMaquina(

    idOtMantencionMaquina           INT IDENTITY(1,1) PRIMARY KEY,
    idMaquina                       INT FOREIGN KEY REFERENCES Maquina(idMaquina),
    idSolicitante                   INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    idRealizador                    INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    descripcionOt                   NVARCHAR(1000) NOT NULL,
    tipoOt                          NVARCHAR(100) NOT NULL,
    fechaSolicitud                  DATETIME,
    fechaRealizada                  DATETIME,
    idTurnoRealizado                INT FOREIGN KEY REFERENCES turno(idturno),
    realizada                       BIT
)


CREATE INDEX IxOtMantencionMaquina ON OtMantencionMaquina(idOtMantencionMaquina);

drop table OtMantencionMaquina;