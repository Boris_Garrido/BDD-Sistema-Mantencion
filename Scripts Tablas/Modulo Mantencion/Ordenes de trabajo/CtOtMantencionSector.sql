CREATE TABLE OtMantencionSector(

    idOtMantencionSector            INT IDENTITY(1,1) PRIMARY KEY,
    idSector                       INT FOREIGN KEY REFERENCES SectorPlanta(idSector),
    idSolicitante                   INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    idRealizador                    INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    descripcionOt                   NVARCHAR(1000) NOT NULL,
    tipoOt                          NVARCHAR(100) NOT NULL,
    fechaSolicitud                  DATETIME,
    fechaRealizada                  DATETIME,
    idTurnoRealizado                INT FOREIGN KEY REFERENCES turno(idturno),
    realizada                       BIT
)


CREATE INDEX IxOtOtMantencionSector ON OtMantencionSector(idOtMantencionSector);
