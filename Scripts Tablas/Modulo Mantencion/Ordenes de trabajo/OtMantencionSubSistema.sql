CREATE TABLE OtMantencionSubSistema(

    idOtMantencionSubSistema        INT IDENTITY(1,1) PRIMARY KEY,
    idSubsistema                    INT FOREIGN KEY REFERENCES Subsistema(idSubSistema),
    idSolicitante                   INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    idRealizador                    INT FOREIGN KEY REFERENCES Usuario(idUsuario),
    descripcionOt                   NVARCHAR(1000) NOT NULL,
    tipoOt                          NVARCHAR(100) NOT NULL,
    fechaSolicitud                  DATETIME,
    fechaRealizada                  DATETIME,
    idTurnoRealizado                INT FOREIGN KEY REFERENCES turno(idturno),
    realizada                       BIT
)

CREATE INDEX IxOtMantencionSubSistema ON OtMantencionSubSistema(idOtMantencionSubSistema);
