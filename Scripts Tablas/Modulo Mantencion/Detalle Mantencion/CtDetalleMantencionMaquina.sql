CREATE TABLE DetalleMantencionEquipo(

    idDetalleMantencionEquipo       INT PRIMARY KEY IDENTITY (1,1),
    idOperador                      INT FOREIGN KEY REFERENCES Operador(idOperador),
    idMantencionEquipo              INT FOREIGN KEY REFERENCES MantencionGeneralMaquina(idMantencionGeneral)
)

CREATE INDEX IxDetalleMantencionEquipo ON DetalleMantencionEquipo(idDetalleMantencionEquipo)