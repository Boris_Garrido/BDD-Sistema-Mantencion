CREATE TABLE DetalleMantencionSector(

    idDetalleMantencionSector       INT PRIMARY KEY IDENTITY (1,1),
    idOperador                      INT FOREIGN KEY REFERENCES Operador(idOperador),
    idMantencionSector              INT FOREIGN KEY REFERENCES MantencionSector(idMantencionSector)
)


select * from Maquina