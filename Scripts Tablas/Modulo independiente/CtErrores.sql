CREATE TABLE errores(
    idError             INT IDENTITY (1,1) PRIMARY KEY,
    procedimiento       VARCHAR(200),
    numeroError         VARCHAR(30),
    descripcionError    NVARCHAR(MAX),
    fecha               DATETIME
)