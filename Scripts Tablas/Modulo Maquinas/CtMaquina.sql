CREATE TABLE Maquina(

    idMaquina                   INT IDENTITY(1,1) PRIMARY KEY,
    idSector                    INT FOREIGN KEY REFERENCES SectorPlanta(idSector),
    tipoMaquina                 VARCHAR(200) NOT NULL,
    nombreEquipo                VARCHAR(200) NOT NULL,
    marca                       VARCHAR(200) NOT NULL,
    AnioFabricacion             VARCHAR(20)     DEFAULT 'Sin Asignar',
    modelo                      VARCHAR(200)    DEFAULT 'Sin Asignar',
    Peso                        VARCHAR(20)     DEFAULT 'Sin Asignar',
    rendimiento                 VARCHAR(200)    DEFAULT 'Sin Asignar',
    fechaRecepcion              VARCHAR(200)    DEFAULT 'Sin Asignar',
    medidas                     VARCHAR(200)    DEFAULT 'Sin Asignar',
    consumoAgua                 VARCHAR(200)    DEFAULT 'Sin Asignar',
    Foto                        VARBINARY(max),
    habilitado                  BIT DEFAULT 1
)


CREATE INDEX IXidMaquina ON Maquina(idMaquina);
