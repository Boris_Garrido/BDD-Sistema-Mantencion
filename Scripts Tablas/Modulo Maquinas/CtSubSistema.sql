CREATE TABLE Subsistema
(
    [idSubSistema]     [int] IDENTITY (1,1) PRIMARY KEY NOT NULL,
    [idMaquina]        [int]                NULL,
    [nombreSubSistema] [varchar](200)       NULL,
    [voltaje]          [varchar](200)       NULL,
    [potencia]         [varchar](200)       NULL,
    [fases]            [varchar](200)       NULL,
    [amperaje]         [varchar](200)       NULL,
    [Frecuencia]       [varchar](200)       NULL,
    [consumoAgua]      [varchar](200)       NULL,
)

CREATE INDEX ixSubSistema ON Subsistema(idSubSistema)