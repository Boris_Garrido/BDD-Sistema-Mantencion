CREATE TABLE Planta(
    idPlanta            INT IDENTITY(1,1) PRIMARY KEY,
    nombrePlanta        VARCHAR(100) NOT NULL,
    direccionPlanta     VARCHAR(200) DEFAULT 'No Asignada',
    telefonoPlanta      VARCHAR(50) DEFAULT 'No Asignado',
    habilitad           BIT
)


CREATE INDEX IxPlantaId ON Planta(idPlanta);

select * from planta;