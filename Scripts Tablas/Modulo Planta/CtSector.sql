CREATE TABLE SectorPlanta(
    idSector            INT IDENTITY (1,1) PRIMARY KEY,
    idPlanta            INT FOREIGN KEY REFERENCES Planta (idPlanta),
    nombreSector        VARCHAR(200),
    Encargado           VARCHAR(200),
    Habilitado          BIT
)

CREATE INDEX IxIdSectorPlanta ON SectorPlanta(idSector)