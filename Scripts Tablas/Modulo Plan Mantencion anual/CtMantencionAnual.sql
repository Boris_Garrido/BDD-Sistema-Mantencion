CREATE SEQUENCE sqPlanMantencionAnual
START WITH 1
INCREMENT BY 1


CREATE TABLE planMantencionAnual(

    idMantencion            INT PRIMARY KEY DEFAULT NEXT VALUE FOR sqPlanMantencionAnual,
    idEquipo                INT FOREIGN KEY REFERENCES Maquina(idMaquina),
    descripcionMantencion   NVARCHAR(1000),
    fechaCreacion           DATETIME,
    areaMantencion          NVARCHAR(200),
    tipoActividad           NVARCHAR(200),
    fechaActualizacion      DATETIME,
    habilitado              BIT
)

CREATE INDEX ixPlanMantencionAnual ON planMantencionAnual(idMantencion)






SELECT

    p.idEquipo                  AS IdEquipo,
    m.nombreEquipo              AS nombreEquipo,
    m.Foto                      AS foto,

    SUM(IIF(mm.idMantencionGeneral IS NULL, 0, 1)) AS actividadRealizada,
    (Select COUNT(*) FROM planMantencionAnual  pt WHERE pt.idEquipo = p.idEquipo ) AS totalActividades
from planMantencionAnual p
LEFT JOIN Maquina m                      ON m.idMaquina       = p.idEquipo
LEFT JOIN MantencionGeneralMaquina mm    ON mm.Descripcion = p.descripcionMantencion AND p.idEquipo = mm.idMaquina
WHERE mm.idMantencionGeneral  IS null
GROUP BY p.idEquipo, m.nombreEquipo,m.Foto

SELECT

    p.idEquipo                  AS IdEquipo,
    p.descripcionMantencion     AS descripcionActividad
from planMantencionAnual p
LEFT JOIN Maquina m                      ON m.idMaquina       = p.idEquipo
LEFT JOIN MantencionGeneralMaquina mm    ON mm.Descripcion = p.descripcionMantencion AND p.idEquipo = mm.idMaquina
WHERE mm.idMantencionGeneral  IS null
AND mm.idMaquina = 1;

select * from Usuario;